<?php

// Réalisation d'une addition !
$var1 = 2;
$var2 = 2;

$result = $var1 + $var2;

var_dump($result);

// Réalisation d'une soustraction !
$var1 = 10;
$var2 = 2;

$result = $var1 - $var2;

var_dump($result);

// Réalisation d'une division !
$var1 = 6;
$var2 = 3;

$result = $var1 / $var2;

var_dump($result);

// Réalisation d'un modulo !
$var1 = 2;
$var2 = 8;

$result = $var1 % $var2;

var_dump($result);
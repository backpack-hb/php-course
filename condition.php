<?php

// Vérification d'une égalité
if(1 == "1"){
  //  echo('Je suis égal ! <br>');
}

$age = 18;

// Avec une clause else
if($age>=18){
   // echo('Tu es majeur !');
} else {
   // echo('Tu es mineur');
}

// Conditions multiple if &&
$age = 17;
$driveLicence = true;
if ($age >= 18 && $driveLicence) {
   // echo('C\'est bon si tu n\'a pas consomé d\'alcool tu peux rouler');
} else {
  //  echo('Désolé, tu ne peux pas rouler');
}

// Condition multiple if ||

if ($age >= 18 || $driveLicence) {
    //echo('C\'est bon si tu n\'a pas consomé d\'alcool tu peux rouler');
} else {
    //echo('Désolé, tu ne peux pas rouler');
}

// SWITCH :

$vitesse = 90;
switch ($vitesse)
{
    case $vitesse > 80:
      //  echo('passe la 5');
        break;
    case $vitesse > 65:
      //  echo('passe la 4');
        break;
    case $vitesse> 40:
       // echo('passe la 3');
        break;
    case $vitesse > 20:
       // echo('passe la 2');
        break;
    default:
      //  echo('Met le point mort');
}

$majeur = false;
$age = 18;


// Equivalent en ternaire :
$majeur = ($age>=18)?true:false;


// var_dump($majeur);


/*
 * LES TABLEAUX
 */

$pannier = [
    'Delirium Tremens',
    'Chouffe'
];

// Affichage du premier élément de mon tableau
// var_dump($pannier);

$pannier[] = 'Fromage';

// var_dump($pannier);

// tableaux


$array = [
    'nom' => 'Delorme',
    'prenom' => 'Aurelien',
    'age' => 28,
    'dateNaissance' => date_create('30-03-1993'),
];
var_dump($array);

var_dump($array['nom']);



?>




<html>
<head>

</head>
</html>

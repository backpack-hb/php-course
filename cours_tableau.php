<?php
$identity = ['nom' => 'Delorme', 'prenom' => 'Aurelien', 'age' => 28, 'dateNaissance' => date_create('30-03-1993')->format('d/m/Y')];

var_dump($identity);

// Affiche le nombre d'éléments de mon tableau
var_dump(count($identity));

// Recherche si la valeur est présente dans le tableau et retourne true ou false
var_dump(in_array('Delorme', $identity));

// Recherche la clé qui a pour valeur Aurélien
$key = array_search('Aurelien', $identity);
var_dump($key);

$listeCourse = ['pomme', 'poire', 'chocolat', 'poire', 'pomme'];

// Ajouter une valeur dans un tableau
array_push($listeCourse, 'Nutella');
$listeCourse[] = 'Toto';

var_dump($listeCourse);

// Création de la variable
$variable = true;
// Suppression de la variable
unset($variable);
// J'affiche ma variable;
var_dump($variable);

// Démo is_null, empty

$chaine = null;

var_dump(empty($chaine));

?>
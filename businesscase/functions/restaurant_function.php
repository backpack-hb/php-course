<?php
// Sert à vérifier l'id de restaurant envoyé dans l'url
function checkUrlRestoParam($bdd){
    // Réccupérer l'id dans mon url
    $idToDelete = $_GET['id'];

    // Vérifier que l'id existe
    $resto = findRestaurantById($idToDelete, $bdd);



    if(is_null($resto)){
        session_start();
        $_SESSION['error'] = 'Ce resto n\'existe pas ICI !';
        http_response_code(404);
        header("Location: error.php");
    } else {
        return $resto;
    }
}


// Fonction qui retourne tous les types de restaurants;
function typeResto(){
    return [
        'Bar',
        'Fast-Food',
        'Gastro'
    ];
}

// Création d'un fichier de fonction pour nos restaurants
// Dedans 2 fonctions : la premiere qui re retrouve un restaurant en fonction de son ID
// La deuxième affiche un card avec le détail d'un restaurant

// Fonction qui réccupére un utilisateur en BDD en fonction de son username
// Retourne le tableau clé => valeur représentant l'utilisateur ou null si l'utilisateur n'existe pas
function findRestaurantById($id, $bdd){
    $query = $bdd->prepare('SELECT * FROM restaurants WHERE id = :id');
    $query->execute(['id' => $id]);
    $result = $query->fetchAll();

    $query->closeCursor();
    if(count($result)>0){
        return $result[0];
    } else {
        return  null;
    }
}

function deleteRestaurant($id, $bdd){

    $query = $bdd->prepare("DELETE FROM restaurants WHERE id = :id");
    try {
        $query->execute(['id'=> $id]);
    } catch (\PDOException $e){
        throw $e;
    }
}



function findRestaurantByName($nom, $bdd){
    $query = $bdd->prepare('SELECT * FROM restaurants WHERE nom = :nom');
    $query->execute(['nom' => $nom]);
    $results = $query->fetchAll();
    return $results;
}

// Dans cette fonction on affiche le détail un élément restaurant qu'elle prend en paramètre
function displayRestaurant($restaurant){

    // On construit notre adresse
    $adresse = $restaurant['num_rue'].' '.$restaurant['nom_rue'].' '.$restaurant['code_postal'].' '.$restaurant['ville'];
    $string = '';

    // Cette variable représentera la bouton à afficher
    $bouton = null;

    /*
     * On remarque dans ces deux boutons que l'on envoie le paramètre id à notre url. Pour que dans le fichier favori.php
     * on puisse savoir quel élément ajouter ou supprimer
     */

    // Le bouton pour supprimer des favoris (on retrouve action = delete notament)
    $boutonSupprimerFavori = ' <a href="favori.php?id='.$restaurant['id'].'&action=delete" class="btn btn-warning">
                                  Supprimmer des favoris !
                                </a>';

    // Le bouton pour ajouter au favori (on note le action = add)
    $boutonAjouterFavori =  ' <a href="favori.php?id='.$restaurant['id'].'&action=add" class="btn btn-success">
                                  Ajouter aux favoris!
                                </a>';

    $boutonSupprimer = '<a  class="btn" href="delete.php?id='.$restaurant['id'].'">Supprimer</a>';

    $boutonEditer = '<a  class="btn" href="resto-edit.php?id='.$restaurant['id'].'">Editer</a>';

    // Si jamais on a une variable "favorite_resto" en session ET que l'on a la valeur du restaurant en paramètre
    if(array_key_exists("favorite_resto", $_SESSION) && in_array($restaurant, $_SESSION['favorite_resto'])){
        // Alors on affiche le bouton pour supprimer des favoris. Ca veut dire que c'est déjà un de nos favori
        $bouton = $boutonSupprimerFavori;
    } else {
        // Alors on affiche le bouton pour ajouter au. Ca veut dire que ce n'est pas déjà un de nos favori
        $bouton = $boutonAjouterFavori;
    }
    echo('  <div class="card m-2 p-2 " style="width: 18rem;">
                            <img src="uploads/photo-resto/'.$restaurant['image_link'].'" class="card-img-top img-thumbnail" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title">'.$restaurant['nom'].'</h5>
                                    <p class="card-text">'.$adresse.'</p>
                                    '.$string.'
                                </div>
                               '.$bouton. $boutonSupprimer.$boutonEditer.'
                            </div>');
}

//  fonction qui va vérifier si un formulaire de restaurant est valide
function validateRestaurantForm($bdd, $restaurant = null){


    $nom = !is_null($restaurant)?$restaurant['nom']: $_POST['nom'];
    $numRue = !is_null($restaurant)?$restaurant['num_rue']: $_POST['num_rue'];
    $email = !is_null($restaurant)?$restaurant['email']: $_POST['email'];
    $type = !is_null($restaurant)?$restaurant['type']: $_POST['type'];


    $errors['isValid'] = true;



    // On vérifie que le champs nom n'est pas vide
    if(empty($nom)){
        $errors['isValid'] = false;
        // Si il est vide, on dit qu'il y a une erreur
        $errors['nom']['error'] = true;
        // On indique la cause de l'erreur
        $errors['nom']['cause'] = 'Veuillez saisir le nom du resto!';
    } else {

        if(is_null($restaurant)){
            // Je selectionne tous les restaurant avec le même nom
            $restoWithName = findRestaurantByName($nom, $bdd);
            // Si il y en a déjà un, j'affiche un message d'erreur disant que ce restaurant existr déjà
            if(count($restoWithName) > 0){
                $errors['isValid'] = false;
                // Si il est vide, on dit qu'il y a une erreur
                $errors['nom']['error'] = true;
                // On indique la cause de l'erreur
                $errors['nom']['cause'] = 'Un restaurant avec le même nom existe déjà';
            } else {
                // Sinon c'est cool il n'y a pas d'erreur
                $errors['nom']['error'] = false;
                // J'ajoute la valeur qui fonctionne
                $errors['nom']['value'] = $nom;
            }
        } else {
            // Sinon c'est cool il n'y a pas d'erreur
            $errors['nom']['error'] = false;
            // J'ajoute la valeur qui fonctionne
            $errors['nom']['value'] = $nom;
        }




    }

    // FIN DE LA FONCTION

// Ici j'ajoute un vérification qui véréfie que c'est bien un format numérique
// Pour vérifier que c'est bien un nombre qui a été saisi
    if(empty($numRue)){

        $errors['isValid'] = false;
        $errors['num_rue']['error'] = true;
        $errors['num_rue']['cause'] = 'Veuillez saisir le numéro de rue';
        // is_numeric retourne true si une chaine de caractère représente un nombre
        // https://www.php.net/manual/fr/function.is-numeric.php
    } elseif(!is_numeric($numRue)) {

        $errors['isValid'] = false;
        $errors['num_rue']['error'] = true;
        $errors['num_rue']['cause'] = 'Veuillez saisir un nombre';
    } else {
        $errors['isValid'] = true;
        $errors['num_rue']['error'] = false;
        $errors['num_rue']['value'] = $numRue;
    }


// Validation du nom de la rue
    if(empty($nomRue)){
        $errors['isValid'] = false;
        $errors['nom_rue']['error'] = true;
        $errors['nom_rue']['cause'] = 'Veuillez saisir le nom de la rue';
        // is_numeric retourne true si une chaine de caractère représente un nombre
        // https://www.php.net/manual/fr/function.is-numeric.php
    } else {
        $errors['nom_rue']['error'] = false;
        $errors['nom_rue']['value'] = $nomRue;
    }


// En plus je vérifie que l'adresse email est bien valide
    if(empty($email)){
        $errors['isValid'] = false;
        $errors['email']['error'] = true;
        $errors['email']['cause'] = 'Veuillez saisir un email';
        // la fonction filter_var permet de vérifier une chaine de caractère.
        // Dans notre cas, on vérifie que l'email est bien valide.
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
        $errors['isValid'] = false;
        $errors['email']['error'] = true;
        $errors['email']['cause'] = 'Votre email n\'est pas valide';
    } else {
        $errors['email']['error'] = false;
        $errors['email']['value'] = $email;
    }

// Vérification du type de restaurant
    if(empty($type)){

        $errors['isValid'] = false;
        $errors['type']['error'] = true;
        $errors['type']['cause'] = 'Un restaurant doit avoir un type';
    } else if (!in_array($type, typeResto())){
        $errors['isValid'] = false;
        $errors['type']['error'] = true;
        $errors['type']['cause'] = 'Arrête de tricher hackeur !';
    }else{
        $errors['isValid'] = true;
        $errors['type']['value'] = $type;
        $errors['type']['error'] = false;
    }


    return ['errors'=> $errors, 'isValid'=> $errors['isValid']];
}



function uploadImageRestaurant($errors)
{
    $errors['isValid'] = true;

// Mon formulaire qui est valide je vais pouvoir uploader mon fichier
    if ($errors['isValid'] || empty($_FILES)) {
        if (empty($_FILES)) {


            $errors['isValid'] = false;
            $errors['errors']['resto_file']['error'] = true;
            $errors['errors']['resto_file']['cause'] = 'On ne peut pas ajouter de restaurants sans images';
        } else {
            $fileToUpload = $_FILES['image'];
            if ($fileToUpload['error'] != 0) {
                $errors['isValid'] = false;
                $errors['errors']['resto_file']['error'] = true;
                $errors['errors']['resto_file']['cause'] = 'Une erreur s\'est produite lors de l\'upload';
            }

            $allowedType = ['image/jpeg', 'image/png'];

            if (!in_array($fileToUpload['type'], $allowedType)) {
                $errors['isValid'] = false;
                $errors['errors']['resto_file']['error'] = true;
                $errors['errors']['resto_file']['cause'] = 'Vous devez ajouter une image JPEG ou PNG';
            }

            $maxSize = 1000000;

            if ($fileToUpload['size'] > $maxSize) {
                $errors['isValid'] = false;
                $errors['errors']['resto_file']['error'] = true;
                $errors['errors']['resto_file']['cause'] = 'Le fichier est trop lourd';
            }


            if ($errors['isValid'] === true) {
                $extensionFile = explode('/', $fileToUpload['type'])[1];
                $uniqFilename = uniqid() . '.' . $extensionFile;

                move_uploaded_file($fileToUpload['tmp_name'], 'uploads/photo-resto/' . $uniqFilename);
            }
        }
    } else {
        $errors['errors']['resto_file']['isValid'] = false;
        $errors['resto_file']['error'] = true;
    }

    if(isset($uniqFilename)){
        $errors['filename'] = $uniqFilename;
    }


    return $errors;
}

function updateRestaurant($id, $bdd){


    $query = $bdd->prepare("UPDATE restaurants
                            SET nom = :nom, num_rue = :num_rue, email = :email, type = :type, nom_rue = :nom_rue
                            WHERE id = :id");
   try {
        $query->execute([
            'nom'=> $_POST['nom'],
            'num_rue'=> $_POST['num_rue'],
            'email'=> $_POST['email'],
            'nom_rue'=> $_POST['nom_rue'],
            'type'=> $_POST['type'],
            'id'=> $id
        ]);
   } catch (\PDOException $e){
       throw  $e;
   }
}


function addRestaurant($bdd, $uniqFilename){
    // Insertion des données seulement si le formulaire est valide !
    $query = $bdd->prepare("INSERT INTO restaurants(nom, num_rue, email, nom_rue, type, image_link)
                            VALUES (:nom, :num_rue, :email, :nom_rue, :type, :image_link)");
    try {
        $query->execute(
            [
                'nom'=> $_POST['nom'],
                'num_rue'=> $_POST['num_rue'],
                'email'=> $_POST['email'],
                'nom_rue'=> $_POST['nom_rue'],
                'type'=> $_POST['type'],
                'image_link'=> $uniqFilename
            ]);
    } catch (\PDOException $e){
        throw $e;
    }

    header('Location: restaurants.php');
}

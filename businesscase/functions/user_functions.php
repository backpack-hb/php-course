<?php
// Fonction qui retourne le booleen true si un utilisateur est connecté.
// Lors de la connexion, la variable super globale $_SESSION est rempli avec une clé username.
// Si on a une variable de session avec la clé username, alors notre utilisateur est connecté.
function isConnected(){
    if(isset($_SESSION['username'])){
        return true;
    } else {
        return false;
    }
}

// Fonction qui réccupére un utilisateur en BDD en fonction de son username
// Retourne le tableau clé => valeur représentant l'utilisateur ou null si l'utilisateur n'existe pas
function findByUsername($username){
    $dbUrl='database'; // le chemin vers le serveur
    $port='3306';
    $dbName='business_case_php'; // le nom de votre base de données
    $user='root'; // nom d'utilisateur pour se connecter
    $password='tiger'; // mot de passe de l'utilisateur pour se connecter

    $connexion = new PDO('mysql:host='.$dbUrl.';port='.$port.';dbname='.$dbName, $user, $password);

    $query = $connexion->prepare('SELECT * FROM utilisateur WHERE username = :username');
    $query->execute(['username' => $username]);

    $result = $query->fetchAll();

    if(count($result)>0){
        return $result[0];
    } else {
        return  null;
    }
}





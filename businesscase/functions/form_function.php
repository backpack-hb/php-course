<?php
function displayError($raison){
        echo('<div id="validationServer05Feedback" class="invalid-feedback">'.$raison.' !!!!</div>');
}

function displayBootstrapClassOnInput($errors, $key){
    // Si le champ nom a une erreur, alors j'ajoute la classe is-invalid à mon input
    if($errors[$key]['error']){
        echo('is-invalid');
    } else {
        echo('is-valid');
    }
}
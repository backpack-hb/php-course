<?php
require 'utils/bdd.php';
require  'functions/restaurant_function.php';

checkUrlRestoParam($bdd);


// Si il existe pas page d'erreur
// On le supprime
deleteRestaurant($idToDelete, $bdd);


// Redirige l'utilisateur

header("Location: restaurants.php");
?>
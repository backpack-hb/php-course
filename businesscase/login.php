<?php
    // J'ai inclu mon fichier qui contient toutes les fonctions liées à l'utilisateur
    require 'functions/user_functions.php';

    // CECI EST UN EXEMPLE de génération d'un hash pour un password

    //$hash = password_hash("admin", PASSWORD_DEFAULT);

    // On cré un tableau
    $errors = [];

    // Si jamais on a soumis le formulaire, on va verifier si on a des erreurs
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        // Le cas ou l'on a pas rempli le username
        if(empty($_POST['username'])){
            $errors[] = 'Veuillez saisir un nom d\'utilisateur';
        }

        // Le cas ou le password n'est pas rempli
        if(empty($_POST['password'])){
            $errors[] = 'Veuillez saisir un mot de passe';
        }

        // Séléctionner mon utilisateur
        $user = findByUsername($_POST['username']);


        if(!$user){
            $errors[] = 'Identifiants incorrects !';
        } else {

            if(password_verify($_POST['password'], $user['password'])){
                $_SESSION['username'] = $_POST['username'];
                $_SESSION['id_user'] = $user['id'];
                // Je redirige l'utilisateur sur ma homepage
                header('Location: restaurants.php');
            } else {
                $errors[] = 'Les identifiants sont incorrectes';
            }
        }
    }
?>
<html>
<head>

</head>
<body>
<!-- Ici je renseigne bien la mèthode utilisé pour que mes données soient envoyés en GET -->
<form method="post">
    <label for="username_input">
        Username
    </label>

    <!-- Ne pas oublier le name sur tous les inputs -->

    <input type="text" name="username" id="username_input" placeholder="username"/>

    <label for="password_input">
        Password
    </label>
    <input type="password" name="password" placeholder="Mot de passe !" id="password_input"/>

    <input type="submit">

    <a href="restaurants.php">Continuer sans compte</a>

    <?php
        // Si le formulaire a été saisi, j'affiche toutes les erreurs.
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            echo('<ul>');
            foreach ($errors as $error){
                echo('<li>'.$error.'</li>');
            }
            echo('</ul>');
        }
    ?>

</form>
</body>
</html>
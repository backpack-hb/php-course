<?php
// N'oubliez pas le session_start()

// Ne pas oublier d'inclure le fichier de fonctions pour les restos
require 'functions/restaurant_function.php';

// De base notre restaurant est nulle
$restaurant = null;

// On vérifie que l'on a bien envoyé un paramètre get id.
// Si c'est le cas on selectionne notre restaurant en BDD
if (key_exists("id", $_GET)) {
    $idAAjouter = $_GET['id'];
    // Appel la fonction findRestaurantById avec en paramètre
    // l'id envoyé dans l'URL
    $restaurant = findRestaurantById($idAAjouter);
}

// Si jamais un utilisateur change l'ID, je renvoie vers une page d'erreur. Je stocke le message de l'erreur
// dans mes variables de session.
if (is_null($restaurant) && isset($_GET['id'])) {
    $_SESSION['error'] = 'Vous faites nimporte quoi ! Ce restaurant n\'existe pas';
    header('Location: error.php');
} else {
    // De base je n'ai pas de resto favori
    $favoriteResto = [];

    // Je vais aller vérifier si je n'en ai pas en session
    if (isset($_SESSION['favorite_resto'])) {
        // Si j'en ai, je vais mettre à jour ma variable $favoriteResto
        $favoriteResto = $_SESSION['favorite_resto'];
    }

    // En fonction de l'action, je vais effectuer le bon traitement
    if (array_key_exists('action', $_GET) && $_GET['action'] != 'delete') {
        // Dans le cas d'un ajout, je vérifie si ce resto n'est pas déjà un de mes préférés
        if (!in_array($restaurant, $favoriteResto) && !is_null($restaurant)) {
            // Je l'ajoute dans mon tableau
            $favoriteResto[] = $restaurant;
        }
        // Le cas d'une suppression
    } else {
        // Je supprime l'élément de mon tableau
        // Fonction php unset supprime une variable
        // La fonction array_search recherche la clé d'un élément dans un tableau
        // 1) Je recherche la clé dans mon tableau
        // 2) Je supprime l'élément de mon tableau qui a cette clé
        unset($favoriteResto[array_search($restaurant, $favoriteResto)]);
    }

    // A la fin du script je met a jour ma session avec mon nouveau tableau
    $_SESSION['favorite_resto'] = $favoriteResto;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- HEAD DE MON APPLICATION
        On retrouve ici tous les fichiers CSS
        On retrouve les métas utiles notement pour le référencement
        Méta title pour afficher le titre dans l'onglet
    !-->

    <?php
    include 'parts/global-head.php';
    ?>

    <title>Mes restaurants favori !</title>
</head>
<body id="page-top">

<!-- Menu de l'application-->
<?php
include('parts/menu.php')
?>
<!-- Header-->
<header class="bg-primary bg-gradient text-white">
    <div class="container px-4 text-center">
        <h1 class="fw-bolder">Bonjour</h1>
        <p class="lead">Nous sommes le <?php echo(date('d/m/Y')); ?></p>
        <a class="btn btn-lg btn-light" href="#about">Timestamp actuel : <?php echo(time());
            $date = date('d/m/Y');
            echo(' | Nous somme le ' . $date);
            $date1 = new DateTime();
            ?> </a>
    </div>
</header>
<!-- About section-->
<section id="about">
    <div class="container px-4">
        <div class="row gx-4 justify-content-center">
            <div class="col-lg-8">
                <div class="row">
                    <h1>Mes restos favori !</h1>
                    <?php

                    if (count($favoriteResto) == 0) {
                        echo('<h1 class="text-danger">Aucun resto préféré</h1>');
                    }
                    foreach ($favoriteResto as $resto) {
                        displayRestaurant($resto);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Services section-->
<section class="bg-light" id="services">
    <div class="container px-4">
        <div class="row gx-4 justify-content-center">
            <div class="col-lg-8">
                <h2>Services we offer</h2>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut optio velit inventore,
                    expedita quo laboriosam possimus ea consequatur vitae, doloribus consequuntur ex. Nemo assumenda
                    laborum vel, labore ut velit dignissimos.</p>
            </div>
        </div>
    </div>
</section>
<!-- Contact section-->
<section id="contact">
    <div class="container px-4">
        <div class="row gx-4 justify-content-center">
            <div class="col-lg-8">
                <h2>Contact us</h2>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero odio fugiat
                    voluptatem dolor, provident officiis, id iusto! Obcaecati incidunt, qui nihil beatae magnam et
                    repudiandae ipsa exercitationem, in, quo totam.</p>
            </div>
        </div>
    </div>
</section>
<!-- Footer-->
<footer class="py-5 bg-dark">
    <div class="container px-4"><p class="m-0 text-center text-white">Copyright &copy; Your Website 2021</p></div>
</footer>


<!-- Avant la fin du body, j'insére tous mes codes JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Core theme JS-->
<script src="js/scripts.js"></script>
<script src="js/fontawesome/all.js"></script>
</body>
</html>

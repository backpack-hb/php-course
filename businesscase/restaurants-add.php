<?php
// Require sur notre BDD
// Ce require doit être sur toutes les pages qui se connectent à la BDD
require 'utils/bdd.php';

// On retrouve ici les fonctions d'affichages du formulaire
require 'functions/form_function.php';

// Les fonctions relatives au restaurant
// Dès que l'on aura des fonctions en relation avec un resto on ajouter ce fichier
require 'functions/restaurant_function.php';


$fileToUpload = null;

// De base on a pas d'erreurs
$errors = null;
$uniqFilename = null;

// On valide notre formulaire
$errors = validateRestaurantForm($bdd);

// On upload notre image et retourne son nom unique
$errors = uploadImageRestaurant($errors);

// Si notre formulaire est valide
if($errors['isValid']){
    $uniqFilename = $errors['filename'];
    // On l'ajoute en BDD
    addRestaurant($bdd, $uniqFilename);
}

$errors = $errors['errors'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- HEAD DE MON APPLICATION
        On retrouve ici tous les fichiers CSS
        On retrouve les métas utiles notement pour le référencement
        Méta title pour afficher le titre dans l'onglet
    !-->

    <?php
    // On a ici tous les css qui sont globaux à toute notre application
    include 'parts/global-head.php';
    ?>

    <title>My PHP Business case !</title>

</head>
<body id="page-top">

<!-- Menu de l'application-->
<?php

include('parts/menu.php')

?>
<!-- Header-->
<header class="bg-primary bg-gradient text-white">
    <div class="container px-4 text-center">
        <h1 class="fw-bolder">Welcome to Scrolling Nav</h1>
        <p class="lead">A functional Bootstrap 5 boilerplate for one page scrolling websites</p>
        <a class="btn btn-lg btn-light" href="#about">Start scrolling!</a>
    </div>
</header>
<!-- About section-->
<section id="about">
    <div class="container px-4">
        <div class="row gx-4 justify-content-center">
            <div class="col-lg-8">
                <div class="row">
                    <h1>Ajouter un restaurant !</h1>


                    <!-- On a ajouté l'attribut method="post" et action = j'ai réutilisé le meme nom de fichier -->

                    <form class="row g-3" method="post" enctype="multipart/form-data" action="restaurants-add.php">


                        <!-- documentation validation des formulaires de bootstrap -->
                        <!-- https://getbootstrap.com/docs/5.0/forms/validation/ -->

                        <!-- Div d'un élément de mon formulaire ici ça concerne le nom du resto -->
                        <div class="col-md-12">
                            <!-- Label de mon formulaire -->
                            <label for="nomRue" class="form-label">Nom</label>
                            <!--Input -->
                            <input type="text" id="nomRue" class="form-control <?php
                                // Cette fonction affiche une classe bootstrap is-valid si le champ nom est valide
                                displayBootstrapClassOnInput($errors, 'nom');
                            ?>" name="nom"
                            <?php
                            // Si il n'y a pas d'erreurs dans le champ, j'affiche l'ancienne valeur
                            // Cela permet à l'utilisateur de ne pas la resaisir.
                                if(!$errors['nom']['error']){
                                    echo("value=\"".$errors['nom']['value']."\"");
                                }
                            ?>
                                   placeholder="Veuillez entrer un nom">

                            <!--Validation -->
                            <?php
                            // Si il y a une erreur, en dessous j'affiche une div de champs invalides
                            if($errors['nom']['error']){
                                displayError($errors['nom']['cause']);
                            }
                            ?>
                        </div>



                        <!-- Div d'un élément de mon formulaire ici ça concerne le numéro de la rue -->
                        <div class="col-md-12">
                            <label for="numRue" class="form-label">Num rue</label>
                            <input type="number" name="num_rue" id="numRue" class="form-control <?php
                                displayBootstrapClassOnInput($errors, 'num_rue');
                            ?>" placeholder="Numéro de rue"  <?php
                            if(!$errors['num_rue']['error']){
                                echo("value=\"".$errors['num_rue']['value']."\"");
                            }
                            ?>
                            />
                            <?php

                            if($errors['num_rue']['error']){
                                displayError($errors['num_rue']['cause']);
                            }
                            ?>
                        </div>


                        <!-- Div d'un élément de mon formulaire ici ça concerne le nom de la rue -->
                        <div class="col-md-12">
                            <label for="nomRue" class="form-label">Nom de la rue</label>
                            <input type="text" name="nom_rue" id="nomRue" class="form-control <?php
                            displayBootstrapClassOnInput($errors, 'num_rue');
                            ?>" placeholder="Nom de la rue"  <?php
                            if(!$errors['nom_rue']['error']){
                                echo("value=\"".$errors['nom_rue']['value']."\"");
                            }
                            ?>
                            />
                            <?php

                            if($errors['nom_rue']['error']){
                                displayError($errors['num_rue']['cause']);
                            }
                            ?>
                        </div>


                        <!-- Div d'un élément de mon formulaire ici ça concerne le nom de la rue -->
                        <div class="col-md-12">
                            <label for="typeResto" class="form-label">Type de restaurant</label>
                            <select name="type" id="typeResto" class="form-control <?php
                                displayBootstrapClassOnInput($errors, 'num_rue');
                            ?>">
                                <option></option>
                                <?php
                                    foreach (typeResto() as $resto){
                                        $stringSelected = ($resto == $_POST['type'])? 'selected':'';
                                        echo('<option '.$stringSelected.' value="'.$resto.'">'.$resto.'</option>');
                                    }
                                ?>

                            </select>
                            <?php

                            if($errors['type']['error']){
                                displayError($errors['type']['cause']);
                            }
                            ?>
                        </div>





                        <!-- Div d'un élément de mon formulaire ici ça concerne l'email du restaurant' -->
                        <div class="col-md-12">
                            <label for="email" class="form-label">Email du restaurant</label>
                            <input type="email" name="email" id="email" class="form-control <?php
                                displayBootstrapClassOnInput($errors, 'num_rue');
                            ?>"
                                <?php
                                if(!$errors['email']['error']){
                                    echo("value=\"".$errors['email']['value']."\"");
                                }
                                ?>

                                   placeholder="Adresse email">

                            <?php
                            if($errors['email']['error']){
                                displayError($errors['email']['cause']);
                            }
                            ?>
                        </div>


                        <!-- chargement de la photo du resto -->

                        <div class="mb-3">
                            <label for="restoPhoto" class="form-label">Photo du restaurant</label>
                            <input name="image" class="form-control <?php
                                displayBootstrapClassOnInput($errors, 'num_rue');
                            ?>" type="file" id="restoPhoto">

                            <?php

                            if($errors['resto_file']['error']){
                                displayError($errors['resto_file']['cause']);
                            }
                            ?>
                        </div>
                        <input type="submit" class="btn btn-primary mt-5"/>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- Services section-->
<section class="bg-light" id="services">
    <div class="container px-4">
        <div class="row gx-4 justify-content-center">
            <div class="col-lg-8">
                <h2>Services we offer</h2>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut optio velit inventore,
                    expedita quo laboriosam possimus ea consequatur vitae, doloribus consequuntur ex. Nemo assumenda
                    laborum vel, labore ut velit dignissimos.</p>
            </div>
        </div>
    </div>
</section>
<!-- Contact section-->
<section id="contact">
    <div class="container px-4">
        <div class="row gx-4 justify-content-center">
            <div class="col-lg-8">
                <h2>Contact us</h2>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero odio fugiat
                    voluptatem dolor, provident officiis, id iusto! Obcaecati incidunt, qui nihil beatae magnam et
                    repudiandae ipsa exercitationem, in, quo totam.</p>
            </div>
        </div>
    </div>
</section>
<!-- Footer-->
<footer class="py-5 bg-dark">
    <div class="container px-4"><p class="m-0 text-center text-white">Copyright &copy; Your Website 2021</p></div>
</footer>


<!-- Avant la fin du body, j'insére tous mes codes JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Core theme JS-->
<script src="js/scripts.js"></script>
<script src="js/fontawesome/all.js"></script>
</body>
</html>
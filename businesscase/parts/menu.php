<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <div class="container px-4">
        <a class="navbar-brand" href="#page-top">Resto'BC</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span
                    class="navbar-toggler-icon"></span></button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ms-auto">
                <li class="nav-item"><a class="nav-link" href="restaurants.php">Les restaurants !</a></li>
                <li class="nav-item"><a class="nav-link" href="restaurants-add.php">Ajouter un restaurant</a></li>
                <li class="nav-item"><a class="nav-link" href="favori.php">Mes favoris !</a></li>


                <?php
                // Ce code permet d'afficher les bons boutons dans le menu.
                // Si le retour de la fonction isConnected user_functions.php est égal à true, j'affiche les boutons Mon compte, Me déconnecter et le nom de l'utilisateur connecté
                if (isConnected()) {
                    echo(' <li class="nav-item"><a class="nav-link" href="account.php">Mon compte</a></li>
                         <li class="nav-item"><a class="nav-link" href="logout.php">Me déconnecter</a></li>
                                <li class="nav-item"><a class="nav-link" href="resas.php">Mes reservations</a></li>');
                    echo('<li class="nav-item"><a class="nav-link" href="account.php">Bonjour ' . $_SESSION['username'] . '</a></li>');

                } else {
                    // Sinon, j'affiche un lien qui envoi vers mon formulaire de connexion
                    echo(' <li class="nav-item"><a class="nav-link" href="login.php">Me connecter</a></li>');
                }
                ?>

            </ul>


            <?php



            if ($_COOKIE['theme'] != 'dark') {

                echo('    
    <label class="form-check-label color-yellow" for="flexSwitchCheckChecked"><i class="fas fa-moon"></i></label>
    
   <div class="form-check form-switch color-orange">
                   <input class="form-check-input" onchange="window.location.href= \'cookie-switch.php?value=dark\'" checked type="checkbox" role="switch" id="flexSwitchCheckChecked">
                   <label class="form-check-label color-orange" for="flexSwitchCheckChecked"><i class="far fa-sun"></i></label>
                </div>');
            } else {
                echo('   <label class="form-check-label color-orange" for="flexSwitchCheckChecked"><i class="far fa-moon"></i></label>
   <div class="form-check form-switch color-orange">
                   <input class="form-check-input" onchange="window.location.href= \'cookie-switch.php?value=white\'" type="checkbox" role="switch" id="flexSwitchCheckChecked">

                   <label class="form-check-label color-yellow" for="flexSwitchCheckChecked"><i class="fas fa-sun"></i></label>
                </div>');
            }
            ?>


        </div>
    </div>

</nav>
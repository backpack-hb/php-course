<?php
    // La page global-head est inclu partout. J'ai donc inclu dans cette page
    // un fichier de fonction qui représentera toutes mes fonctions utilisateurs
    require 'functions/user_functions.php';

if(!array_key_exists('theme', $_COOKIE)){
    $_COOKIE['theme'] = 'white';
    setcookie("theme", "white");
}

?>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
<meta name="description" content=""/>
<meta name="author" content=""/>
<link rel="icon" type="image/x-icon" href="assets/favicon.ico"/>
<link href="css/style.css" rel="stylesheet"/>
<link href="css/color.css" rel="stylesheet"/>

<?php
    if($_COOKIE['theme'] == 'dark'){
        echo('<link href="css/dark.css" rel="stylesheet"/>');
    } else {
        echo('<link href="css/white.css" rel="stylesheet"/>');
    }
?>

<link href="css/fontawesome/all.css" rel="stylesheet"/>
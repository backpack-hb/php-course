<!DOCTYPE html>
<html lang="en">
<head>
    <!-- HEAD DE MON APPLICATION
        On retrouve ici tous les fichiers CSS
        On retrouve les métas utiles notement pour le référencement
        Méta title pour afficher le titre dans l'onglet
    !-->

    <?php

    require 'utils/bdd.php';
    require 'functions/restaurant_function.php';
    require 'functions/form_function.php';
    include 'parts/global-head.php';

    $restaurant = findRestaurantById($_GET['id']);

    // De base on a pas d'erreurs

    $errors = null;
    $valid = true;

    // On vérifie que le champs nom n'est pas vide
    if(empty($_POST['nom'])){
        $valid = false;
        // Si il est vide, on dit qu'il y a une erreur
        $errors['nom']['error'] = false;
        // On indique la cause de l'erreur
        $errors['nom']['cause'] = 'Veuillez saisir le nom du resto!';
    } else {
        // Sinon c'est cool il n'y a pas d'erreur
        $errors['nom']['error'] = false;
    }

    // Ici j'ajoute un vérification qui véréfie que c'est bien un format numérique
    // Pour vérifier que c'est bien un nombre qui a été saisi
    if(empty($_POST['num_rue'])){
        $valid = false;
        $errors['num_rue']['error'] = false;
        // is_numeric retourne true si une chaine de caractère représente un nombre
        // https://www.php.net/manual/fr/function.is-numeric.php
    } elseif(!is_numeric($_POST['num_rue'])) {
        $errors['num_rue']['error'] = false;
    } else {
        $errors['num_rue']['error'] = false;
    }

    // En plus je vérifie que l'adresse email est bien valide
    if(empty($_POST['email'])){
        $valid = false;
        $errors['email']['error'] = false;
        // la fonction filter_var permet de vérifier une chaine de caractère.
        // Dans notre cas, on vérifie que l'email est bien valide.
    } elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        $errors['email']['error'] = false;
    } else {
        $errors['email']['error'] = false;
    }



    if($valid){
        require 'utils/bdd.php';
        $req = $bdd->prepare(
            'UPDATE restaurants SET nom = :nom, num_rue = :num_rue, email = :email WHERE id = :id');
        $req->execute([
            'nom' => $_POST['nom'],
            'num_rue' => $_POST['num_rue'],
            'email' => $_POST['email'],
            'id'=> $restaurant['id']
        ]);
        header("Location: restaurants.php");
    }


    ?>

    <title>My PHP Business case !</title>

</head>
<body id="page-top">

<!-- Menu de l'application-->
<?php
include('parts/menu.php')
?>
<!-- Header-->
<header class="bg-primary bg-gradient text-white">
    <div class="container px-4 text-center">
        <h1 class="fw-bolder">Welcome to Scrolling Nav</h1>
        <p class="lead">A functional Bootstrap 5 boilerplate for one page scrolling websites</p>
        <a class="btn btn-lg btn-light" href="#about">Start scrolling!</a>
    </div>
</header>
<!-- About section-->
<section id="about">
    <div class="container px-4">
        <div class="row gx-4 justify-content-center">
            <div class="col-lg-8">
                <div class="row">
                    <h1>Editer le restaurant <?php echo($restaurant['nom']) ?> !</h1>


                    <!-- On a ajouté l'attribut method="post" et action = j'ai réutilisé le meme nom de fichier -->

                    <form class="row g-3" method="post" action="restaurant-edit.php?id=<?php echo($_GET["id"]);?>">


                        <!-- documentation validation des formulaires de bootstrap -->
                        <!-- https://getbootstrap.com/docs/5.0/forms/validation/ -->

                        <!-- Div d'un élément de mon formulaire ici ça concerne le nom du resto -->
                        <div class="col-md-12">
                            <!-- Label de mon formulaire -->
                            <label for="nomRue" class="form-label">Nom</label>
                            <!--Input -->
                            <input type="text" id="nomRue" class="form-control <?php
                            // Si le champ nom a une erreur, alors j'ajoute la classe is-invalid à mon input
                            if($errors['nom']['error']){
                                echo('is-invalid');
                            } else {
                                echo('is-valid');
                            }
                            ?>" name="nom"
                                <?php
                                // Si il n'y a pas d'erreurs dans le champ, j'affiche l'ancienne valeur
                                // Cela permet à l'utilisateur de ne pas la resaisir.

                                    echo("value=\"".$restaurant['nom']."\"");
                                ?>
                                   placeholder="Veuillez entrer un nom">

                            <!--Validation -->
                            <?php

                            // Si il y a une erreur, en dessous j'affiche une div de champs invalides
                            if($errors['nom']['error']){
                                displayError($errors['nom']['cause']);
                            }
                            ?>
                        </div>



                        <!-- Div d'un élément de mon formulaire ici ça concerne le numéro de la rue -->
                        <div class="col-md-12">
                            <label for="numRue" class="form-label">Num rue</label>
                            <input type="number" name="num_rue" id="numRue" class="form-control <?php
                            if($errors['num_rue']['error']){
                                echo('is-invalid');
                            } else {
                                echo('is-valid');
                            }
                            ?>" placeholder="Numéro de rue" value="<?php echo($restaurant['num_rue'])?>"
                            />
                            <?php

                            if($errors['num_rue']['error']){
                                displayError($errors['num_rue']['cause']);
                            }
                            ?>
                        </div>

                        <!-- Div d'un élément de mon formulaire ici ça concerne l'email du restaurant' -->
                        <div class="col-md-12">
                            <label for="email" class="form-label">Email du restaurant</label>
                            <input type="email" name="email" id="email" class="form-control <?php
                            if($errors['email']['error']){
                                echo('is-invalid');
                            } else {
                                echo('is-valid');
                            }
                            ?>"
                                <?php

                                    echo("value=\"".$restaurant['email']."\"");
                                ?>

                                   placeholder="Adresse email">

                            <?php
                            if($errors['email']['error']){
                                displayError($errors['nom']['cause']);
                            }
                            ?>
                        </div>

                        <input type="submit" class="btn btn-primary mt-5"/>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- Services section-->
<section class="bg-light" id="services">
    <div class="container px-4">
        <div class="row gx-4 justify-content-center">
            <div class="col-lg-8">
                <h2>Services we offer</h2>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut optio velit inventore,
                    expedita quo laboriosam possimus ea consequatur vitae, doloribus consequuntur ex. Nemo assumenda
                    laborum vel, labore ut velit dignissimos.</p>
            </div>
        </div>
    </div>
</section>
<!-- Contact section-->
<section id="contact">
    <div class="container px-4">
        <div class="row gx-4 justify-content-center">
            <div class="col-lg-8">
                <h2>Contact us</h2>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero odio fugiat
                    voluptatem dolor, provident officiis, id iusto! Obcaecati incidunt, qui nihil beatae magnam et
                    repudiandae ipsa exercitationem, in, quo totam.</p>
            </div>
        </div>
    </div>
</section>
<!-- Footer-->
<footer class="py-5 bg-dark">
    <div class="container px-4"><p class="m-0 text-center text-white">Copyright &copy; Your Website 2021</p></div>
</footer>


<!-- Avant la fin du body, j'insére tous mes codes JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Core theme JS-->
<script src="js/scripts.js"></script>
<script src="js/fontawesome/all.js"></script>
</body>
</html>
<?php
    require 'utils/bdd.php';
    $id = $_GET['id'];

    $res = $bdd->prepare('DELETE FROM restaurants WHERE id = :id');
    $res->execute(['id'=> $id]);

    header('Location: restaurants.php');
?>
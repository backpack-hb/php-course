<?php
    // CONFIG
    $dbName = 'business_case_php';
    $host = 'database';
    $port = '3306';
    $user = 'root';
    $password = 'tiger';

// Connexion à notre BDD
// Le try catch nous permet de lancer une erreur si la connexion échoue
try {
    $bdd = new PDO(
        'mysql:host='.$host.';port='.$port.';dbname='.$dbName.';charset=utf8',
        $user,
        $password);
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (\PDOException $e){
    throw $e;
}
?>
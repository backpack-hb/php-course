<?php
require 'utils/bdd.php';
require 'functions/restaurant_function.php';

$type = null;

if(!empty($_GET) && $_GET['type']){
    $type = $_GET['type'];
}

require 'functions/form_function.php';


// Je prépare mon serveur PHP à envoyer une requête SQL à ma BDD (celle que l'on a créé a la ligne 17)
$query = null;
if(is_null($type)){
    $query = $bdd->prepare('SELECT * FROM restaurants');
    $query->execute();
} else {

    $query = $bdd->prepare('SELECT * FROM restaurants WHERE type = :type');
    $query->execute(['type' => $type]);
}

// Réccupérer tous les résultats de ma requête sous forme de tableau
// $arrayBar = $query->fetchAll();

?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <!-- HEAD DE MON APPLICATION
            On retrouve ici tous les fichiers CSS
            On retrouve les métas utiles notement pour le référencement
            Méta title pour afficher le titre dans l'onglet
        !-->

        <?php
            include 'parts/global-head.php';
        ?>

        <title>My PHP Business case !</title>
    </head>
    <body id="page-top">

    <!-- Menu de l'application-->
    <?php
        include('parts/menu.php')
    ?>
    <!-- Header-->
    <header class="bg-primary bg-gradient text-white">
        <div class="container px-4 text-center">
            <h1 class="fw-bolder">Bonjour</h1>
            <p class="lead">Nous sommes le <?php echo(date('d/m/Y'));?></p>
            <a class="btn btn-lg btn-light" href="#about">Timestamp actuel : <?php echo(time());
                $date = date('d/m/Y');
                echo(' | Nous somme le '. $date);
                $date1 = new DateTime();
                ?> </a>
        </div>
    </header>
    <!-- About section-->
    <section id="about">
        <div class="container px-4">
            <div class="row gx-4 justify-content-center">
                <div class="col-lg-8">
                    <div class="row">

                        <a class="col-2" href="restaurants.php">
                            <button class="btn btn-success">Tous les voirs</button>
                        </a>

                        <?php
                        foreach (typeResto() as $type){
                            echo('  <a class="col-2" href="restaurants.php?type='.$type.'">
                            <button class="btn btn-success">Voir les '.$type.'</button></a>');
                        }
                        ?>

                        <?php
                        if(empty($_GET) || !isset($_GET['type'])){
                            echo('<h1> Les  restaurants de Resto\'BC !</h1>');
                        } else {
                            echo('<h1> Les  restaurants '.$_GET["type"].' de Resto\'BC !</h1>');
                        }


                        while ($resto = $query->fetch()) {
                           displayRestaurant($resto);
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Services section-->
    <section class="bg-light" id="services">
        <div class="container px-4">
            <div class="row gx-4 justify-content-center">
                <div class="col-lg-8">
                    <h2>Services we offer</h2>
                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut optio velit inventore,
                        expedita quo laboriosam possimus ea consequatur vitae, doloribus consequuntur ex. Nemo assumenda
                        laborum vel, labore ut velit dignissimos.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact section-->
    <section id="contact">
        <div class="container px-4">
            <div class="row gx-4 justify-content-center">
                <div class="col-lg-8">
                    <h2>Contact us</h2>
                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero odio fugiat
                        voluptatem dolor, provident officiis, id iusto! Obcaecati incidunt, qui nihil beatae magnam et
                        repudiandae ipsa exercitationem, in, quo totam.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- Footer-->
    <footer class="py-5 bg-dark">
        <div class="container px-4"><p class="m-0 text-center text-white">Copyright &copy; Your Website 2021</p></div>
    </footer>


    <!-- Avant la fin du body, j'insére tous mes codes JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
    <script src="js/fontawesome/all.js"></script>
    </body>
    </html>
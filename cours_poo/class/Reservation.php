<?php
    class Reservation{
        private $id;
        private $user;
        private $restaurant;
        private $dateReservation;

        public function __construct($user, $restaurant, $dateReservationn, $id=null)
        {
            $this->id = $id;
            $this->user = $user;
            $this->restaurant = $restaurant;
            $this->dateReservation = $dateReservationn;
        }

        public function getId(): mixed
        {
            return $this->id;
        }

        public function setId(mixed $id): void
        {
            $this->id = $id;
        }

        public function getUser()
        {
            return $this->user;
        }

        public function setUser($user): void
        {
            $this->user = $user;
        }

        public function getRestaurant()
        {
            return $this->restaurant;
        }

        public function setRestaurant($restaurant): void
        {
            $this->restaurant = $restaurant;
        }

        public function getDateReservatio()
        {
            return $this->dateReservatio;
        }

        public function setDateReservatio($dateReservation): void
        {
            $this->dateReservatio = $dateReservation;
        }
    }
?>
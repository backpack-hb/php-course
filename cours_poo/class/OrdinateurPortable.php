<?php
class OrdinateurPortable extends Ordinateur {
    private $displaySize;

    public function __construct($displaySize, $memoireRam, $tailleDisqueDur, $hasSsd)
    {
        parent::__construct($memoireRam, $tailleDisqueDur, $hasSsd);
        $this->displaySize = $displaySize;
    }

    public function getRam(){
        return $this->memoireRam;
    }
}
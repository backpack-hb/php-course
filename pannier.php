<?php
$elementsPanier = [
    [
        'nom' => 'Farine',
        'quantity' => '2kg', 'image' =>
        'https://cdn.monoprix.fr/cdn-cgi/image/width=580,quality=60,format=auto,metadata=none/public/images/grocery/1060/580x580.jpg',
        'color'=> 'cafe'
    ],
    [
        'nom' => 'Nutella',
        'quantity' => '1kg',
        'image' => 'https://www.cdiscount.com/pdt2/2/4/5/1/700x700/nut8000500150245/rw/nutella-pate-a-tartiner-350-g.jpg',
        'color'=> 'color-gastro'
    ],
    [
        'nom' => 'Biere',
        'quantity' => '1l',
        'image' => 'https://media.gqmagazine.fr/photos/5f7ddd8898c274a8707da089/16:9/w_2560%2Cc_limit/GettyImages-684133728.jpg',
        'color'=> 'fast-food'
    ],
]
?>
<html>
<head>
    <link rel="stylesheet" href="businesscase/css/color.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <h1>Mon panier !</h1>

    <div class="row">
        <?php
            foreach ($elementsPanier as $element){
                $classs = '';
                if($element['color'] == 'fast-food'){

                    $classs = 'fast-food';
                }
                echo('<div class="card m-5 '.$classs.'" style="width: 18rem;">
                        <img src="'.$element['image'].'" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">'.$element['nom'].'</h5>
                        <p class="card-text"><u>Quantité : </u> '.$element['quantity'].'</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>');
            }

            var_dump($elementsPanier);
        ?>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>
</html>
<?php
    class CategoryController extends AuthenticationController {

        private $categManager;

        public function __construct()
        {
            parent::__construct();
            $this->categManager = new CategoryManager();
        }

        public function list(){
            $categories = $this->categManager->getAllFull();


            require 'Vue/categories/listing.php';
        }

        public function add(){

            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $category = new Category($_POST['nom']);
                $errors = $this->formValidation($category);

                if($errors['globalValidation']){
                    $this->categManager->add($category);
                    header("Location: index.php?controller=category&action=list");
                }

            }

            if(isset($errors)){
                $errors = $errors['errors'];
            }

            require 'Vue/categories/add.php';
        }

        public function edit($id){
            $category = $this->categManager->findOne($id);

            $errors = $this->formValidation($category);

            if($_SERVER["REQUEST_METHOD"] == 'POST'){
                $category->setNom($_POST["nom"]);

                $this->categManager->update($category);

                header("Location: index.php?controller=category&action=list");
            }


            require "Vue/categories/edit.php";
        }

        public function remove($id){
            $this->categManager->remove($id);
            header("Location: index.php?controller=category&action=list");
        }

        private function formValidation(Category $category){
            $errors = [];
            $globalValidation = true;

            // Valider les données

            if (empty($category->getNom())) {
                $errors['nom']['valid'] = false;
                $errors['nom']['cause'] = 'Veuillez saisir un nom';
                $globalValidation = false;
            } elseif (!is_null($this->categManager->findByNom($category->getNom()))){
                $errors['nom']['valid'] = false;
                $errors['nom']['cause'] = 'Cette category existe déjà !';
                $globalValidation = false;
            }

            else {
                $errors['nom']['valid'] = true;
                $errors['nom']['value'] = $category->getNom();
            }

            return ['errors'=> $errors, 'globalValidation'=> $globalValidation];
        }
    }
?>
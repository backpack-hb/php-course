<?php
class AuthenticationController{
    protected $user;
    protected $session;

    public function __construct(){
        $this->session = new Session();
        if(isset($this->session->utilisateur)){
            $this->user = unserialize($this->session->utilisateur);
        } else {
            header("Location: index.php?controller=security&action=login");
        }
    }
}
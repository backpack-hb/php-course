<?php
    class SecurityController{

        private $utilisateurManager;
        private $sessionManager;

        public function __construct(){
            $this->utilisateurManager = new UtilisateurManager();
            $this->sessionManager = new Session();
        }

        public function login(){
            $errors = [];
            if($_SERVER['REQUEST_METHOD'] == 'POST') {
                $utilisateur = $this->utilisateurManager->findByUsername($_POST["login"]);
                if(is_null($utilisateur)){
                    $errors[] = 'Identifiant ou mot de passe incorrecte';
                } else {
                    if(password_verify($_POST['password'], $utilisateur->getPassword())){
                        $userString = serialize($utilisateur);
                        $this->sessionManager->utilisateur = $userString;

                        header('Location: index.php?controller=restaurant&action=list');
                    } else {
                        $errors[] = "Identifiant ou mot de passe incorrecte";
                    }
                }

            }

            require 'Vue/Security/login.php';
        }

        public function register(){

            if($_SERVER['REQUEST_METHOD'] == 'POST'){

                $errors = [];
                $globalValidation = true;

                // Valider les données

                if (empty($_POST['username'])) {
                    $errors['username']['valid'] = false;
                    $errors['username']['cause'] = 'Veuillez saisir un nom d\'utilisateur';
                    $globalValidation = false;
                } else {
                    $errors['username']['valid'] = true;
                    $errors['username']['value'] = $_POST['username'];

                    if($this->utilisateurManager->findByUsername($_POST['username'])){
                        $errors['username']['valid'] = false;
                        $errors['username']['cause'] = "Cet utilisateur existe déjà !";
                        $globalValidation = false;
                    }
                }


                if(empty($_POST['password'])){
                    $errors['password']['valid'] = false;
                    $errors['password']['cause'] = 'Veuillez saisir un mot de passe';
                    $globalValidation = false;
                } else {
                    if($_POST['password_confirm'] != $_POST['password_confirm']){
                        $errors['password_confirm']['valid'] = false;
                        $errors['password_confirm']['cause'] = 'Les 2 mots de passes ne correspondent pas !';
                        $globalValidation = false;
                    } else {
                        $errors['password']['valid'] = true;
                    }
                }

                if(empty($_POST['nom'])){
                    $errors['nom']['valid'] = false;
                    $errors['nom']['cause'] = 'Veuillez saisir un nom';
                    $globalValidation = false;
                } else {
                    $errors['nom']['valid'] = true;
                    $errors['nom']['value'] = $_POST['username'];
                }

                if(empty($_POST['prenom'])){
                    $errors['prenom']['valid'] = false;
                    $errors['prenom']['cause'] = 'Veuillez saisir un prenom';
                    $globalValidation = false;
                } else {
                    $errors['prenom']['valid'] = true;
                    $errors['prenom']['value'] = $_POST['username'];
                }

                if(empty($_POST['email'])){
                    $errors['email']['valid'] = false;
                    $errors['email']['cause'] = 'Veuillez saisir un email';
                    $globalValidation = false;
                } else {
                    if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                        $errors['email']['valid'] = true;
                        $errors['email']['value'] = $_POST['email'];
                    } else {
                        $errors['email']['valid'] = false;
                        $errors['email']['cause'] = "L'email n'est pas au format mail";
                        $globalValidation = false;
                    }
                }

                if($globalValidation){
                    $allowedType = ['image/jpeg', 'image/png'];

                    if($_FILES['profile_picture']['size'] == 0){
                        $errors['profile_picture']['valid'] = false;
                        $errors['profile_picture']['cause'] = "Veuillez uploader une image !";
                        $globalValidation = false;
                    } else {

                        if($_FILES['profile_picture']['error'] != 0){
                            $errors['profile_picture']['valid'] = false;
                            $errors['profile_picture']['cause'] = "Erreur lors du chargement de l'image !";
                            $globalValidation = false;
                        } else if(!in_array($_FILES['profile_picture']['type'], $allowedType)) {
                            $errors['profile_picture']['valid'] = false;
                            $errors['profile_picture']['cause'] = "Ce type de fichier n'est pas autorisé !";
                            $globalValidation = false;
                        } else if($_FILES['profile_picture']['size']>10000000){
                            $errors['profile_picture']['valid'] = false;
                            $errors['profile_picture']['cause'] = "Votre photo est trop lourde !";
                            $globalValidation = false;
                        } else {

                            $extension = explode('/', $_FILES['profile_picture']['type'])[1];
                            $fileName = uniqid().'.'.$extension;

                            move_uploaded_file($_FILES['profile_picture']['tmp_name'], 'Public/images/profile_picture/'.$fileName);


                            $utilisateur = new Utilisateur($_POST['username'], password_hash($_POST['password'], PASSWORD_DEFAULT), $_POST["nom"], $_POST["prenom"],
                                $_POST["email"], $fileName);

                            $this->utilisateurManager->add($utilisateur);

                            header("Location: index.php?controller=security&action=login");
                        }
                    }


                }
            }

            require 'Vue/Security/register.php';
        }

        public function logout(){
            session_destroy();
            header("Location: index.php?controller=security&action=login");
        }
    }
?>
<?php
// Toutes les requêtes HTTP relatives à un restaurant sont traitées dans ce controleur.
// On retrouvera une méthode pour chacune des pages relatives au restaurant
// Le role du controlleur sera d'aller récolter les données ci besoin dans la couche modele
// De retourner une vue vue dans le cas ou l'on a quelque chose à afficher
// De retourner une redirection
class RestaurantController extends AuthenticationController
{

    // Pour pouvoir récolter des données, notre controleur aura besoin d'un manager
    // Le role du manager sera de selectionner des résultats en BDD puis de retourner des objets
    private $restaurantManager;

    // Un restaurant est lié à une category
    // Dans le formulaire d'ajout il va falloir que je réccupére toutes les categories présentes en BDD
    // afin que l'utilisateur puisse choisir la catégorie en question
    private $categoryManager;

    // On initialise notre attribut restaurantManager.
    public function __construct()
    {
        parent::__construct();
        // Une fois que cela sera fait nous pourrons appeler toutes les méthodes du restaurant manager
        $this->restaurantManager = new RestaurantManager();
        $this->categoryManager = new CategoryManager();
    }

    // Cette méthode est appelée quand j'ai dans mon url index.php?controller=restaurant&action=list
    public function listRestaurant()
    {
        // Je demande à mon manager de réccupérer tous les objets restaurants présents en BDD
        $restos = $this->restaurantManager->getAll();

        // J'inclu la vue relative elle contiendra un tableau HTML qui liste tous mes restos.
        require "Vue/restaurants/listing.php";
    }

    // Cette méthode est lancée par le routeur (index.php) quand on accède a une URL
    // qui contient ?controller=restaurant&action=detail&id=unId
    public function detail($id)
    {
        // On va demander ici à notre manager de selectionner un objet restaurant en fonction de son id
        // Il va nous retourner un objet restaurant que nous allons stocker dans la variable resto
        // Cette fonction prend en paramètre un id (l'id du resto que l'on recherche)
        $resto = $this->restaurantManager->getOneFull($id);

        // Il retourne la vue de détail
        require "Vue/restaurants/detail.php";
    }

    public function add()
    {
        $categories = $this->categoryManager->getAll();


        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $categ = $this->categoryManager->findOne($_POST["category"]);

            $restaurant = new Restaurant($_POST['nom'], $_POST['numeroRue'],
                $_POST['nomRue'], $_POST['ville'], $categ);

            $errors = $this->formValidation($restaurant);
            if ($errors['globalValidation']) {
                $this->restaurantManager->add($restaurant);
                header("Location: index.php?controller=restaurant&action=list");
            }
        }


        if(isset($errors)){
            $errors = $errors['errors'];
        }


        require 'Vue/restaurants/add.php';
    }

    public function edit($id){
        $restaurant = $this->restaurantManager->getOne($id);


        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $restaurant->setNom($_POST['nom']);
            $restaurant->setNomRue($_POST['nomRue']);
            $restaurant->setNumeroRue($_POST['numeroRue']);
            $restaurant->setVille($_POST['ville']);

            $errors = $this->formValidation($restaurant);

            if($errors['globalValidation']){
                $this->restaurantManager->edit($restaurant);
                header('Location: index.php?controller=restaurant&action=list');
            }
        }

        if(isset($errors)){
            $errors = $errors['errors'];
        }

        require 'Vue/restaurants/edit.php';
    }

    public function remove($id)
    {
        //Supprimer mon élément
        $this->restaurantManager->remove($id);
        // Rediriger l'utilisateur vers le listing
        header('Location: index.php?controller=restaurant&action=list');
    }

    private function formValidation(Restaurant $restaurant){
        $errors = [];
        $globalValidation = true;

        // Valider les données

        if (empty($restaurant->getNom())) {
            $errors['nom']['valid'] = false;
            $errors['nom']['cause'] = 'Veuillez saisir un nom';
            $globalValidation = false;
        } else {
            $errors['nom']['valid'] = true;
            $errors['nom']['value'] = $restaurant->getNom();
        }

        if (empty($restaurant->getNumeroRue())) {
            $errors['numeroRue']['valid'] = false;
            $errors['numeroRue']['cause'] = 'Veuillez saisir un numéro de rue';
            $globalValidation = false;
        } else {
            if (!is_numeric($_POST['numeroRue'])) {
                $errors['numeroRue']['valid'] = false;
                $errors['numeroRue']['cause'] = 'Un numéro de rue ne peut contenir que des chiffres';
                $globalValidation = false;
            } else {
                $errors['numeroRue']['valid'] = true;
                $errors['numeroRue']['value'] = $restaurant->getNumeroRue();
            }
        }

        if (empty($restaurant->getNomRue())) {
            $errors['nomRue']['valid'] = false;
            $errors['nomRue']['cause'] = 'Veuillez saisir un nom de rue';
            $globalValidation = false;
        } else {
            $errors['nomRue']['valid'] = true;
            $errors['nomRue']['value'] = $restaurant->getNomRue();
        }

        if (empty($restaurant->getVille())) {
            $errors['ville']['valid'] = false;
            $errors['ville']['cause'] = 'Veuillez saisir une ville';
            $globalValidation = false;
        } else {
            $errors['ville']['valid'] = true;
            $errors['ville']['value'] = $restaurant->getVille();
        }

        if(is_null($restaurant->getCategory())){
            $errors['category']['valid'] = false;
            $errors['category']['cause'] = 'Vous devez selectionner une category !';
            $globalValidation = false;
        } else {
            $errors['category']['valid'] = true;
            $errors["category"]["value"] = $restaurant->getCategory()->getId();
        }

        return ['errors'=> $errors, 'globalValidation'=> $globalValidation];
    }

}
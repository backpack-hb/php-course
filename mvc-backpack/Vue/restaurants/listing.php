<html>
<head>
    <?php include 'Vue/parts/global/global-stylesheets.php'; ?>
    <?php include 'Vue/parts/table/table-stylesheets.php'; ?>
</head>
<body>
<div class="container">
    <?php
    include 'Vue/parts/menu.php'
    ?>


    <h1>Les restaurants de l'appli !</h1>

    <a href="index.php?controller=restaurant&action=add">Ajouter un resto !</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nom</th>
            <th scope="col">Adresse</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        // Seul PHP de la vue : Affichage de mon tableau de restaurant..
        foreach ($restos as $resto) {
            $adresse = $resto->getNumeroRue() . ' ' . $resto->getNomRue() . ' ' . $resto->getVille();
            echo(' <tr>
            <td scope="row">' . $resto->getId() . '</td>
            <td>' . $resto->getNom() . '</td>
            <td >' . $adresse . '</td>
            <td>
                <!-- 
                En MVC On ne doit pas faire de liens ailleurs que sur le fichier index.php
                Je rajoute mon lien vers le fichier index.php. 
                Je lui donne le controlleur restaurant et l\'action détail. Je lui passe le paramètre ID 
                du restaurant à afficher.       
                 -->
                 <a href="index.php?controller=restaurant&action=detail&id=' . $resto->getId() . '">
                 
                 <button class="btn btn-success">
                    Voir en détail
                </button>
                
                </a>
                
                <a href="index.php?controller=restaurant&action=edit&id=' . $resto->getId() . '">
                 
                 <button class="btn btn-warning">
                    Editer '.$resto->getNom().'
                </button>
                
                </a>
                <button onclick="changeModalLink('.$resto->getId().')" data-bs-toggle="modal" data-bs-target="#modalDelete"class="btn btn-danger">Supprimer</button>
            </td>
        </tr>');
        }
        ?>

        </tbody>
    </table>


    <div class="modal" id="modalDelete" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Supprimer un restaurant</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Action irrémédiable !</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <a id="deleteButtonModal">
                        <button id="" type="button" class="btn btn-danger">Save changes</button>
                    </a>

                </div>
            </div>
        </div>
    </div>


    <?php
    include 'Vue/parts/footer.php'
    ?>

    <?php
        include 'Vue/parts/global/global-scripts.php';
    ?>

    <?php
        include 'Vue/parts/table/table-scripts.php';
    ?>

</div>
</body>
</html>
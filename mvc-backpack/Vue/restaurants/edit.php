<html>
<head>
    <?php include 'Vue/parts/global/global-stylesheets.php'; ?>
</head>
<body>
<div class="container">
    <?php
    include 'Vue/parts/menu.php'
    ?>

    <h1>Editer le restaurant <?php echo($restaurant->getNom());?> !</h1>

    <a href="index.php?controller=restaurant&action=list">Revenir en arrière !</a>


    <form class="row g-3" method="post">
        <div class="col-md-12">
            <label for="nomRestaurant" class="form-label">Nom du restaurant</label>
            <input type="text" value="<?php echo($restaurant->getNom()) ?>" name="nom" class="form-control <?php
            if(!isset($errors) || $errors['nom']['valid']){
                echo('is-valid');
            } else {
                echo('is-invalid');
            }
            ?>"
                <?php
                if(isset($errors) && $errors['nom']['valid']){
                    echo('value="'.$errors['nom']['value'].'"');
                }
                ?> id="nomRestaurant">
            <div class="invalid-feedback">
                <?php
                if(isset($errors) && !$errors['nom']['valid']){
                    echo($errors['nom']['cause']);
                }?>
            </div>
        </div>
        <div class="col-md-12">
            <label for="numeroRue" class="form-label">Numéro de la rue</label>
            <input type="number" value="<?php echo($restaurant->getNumeroRue()) ?>" name="numeroRue" class="form-control <?php
            if(!isset($errors) || $errors['numeroRue']['valid']){
                echo('is-valid');
            } else {
                echo('is-invalid');
            }
            ?>"
                <?php
                if(isset($errors) && $errors['numeroRue']['valid']){
                    echo('value="'.$errors['numeroRue']['value'].'"');
                }?>
                   id="numeroRue">
            <div class="invalid-feedback">
                <?php
                if(isset($errors) && !$errors['numeroRue']['valid']){
                    echo($errors['numeroRue']['cause']);
                }?>
            </div>
        </div>

        <div class="col-md-12">
            <label for="nomRue" class="form-label">Nom de la rue</label>
            <input type="text" value="<?php echo($restaurant->getNomRue());?>" name="nomRue" class="form-control <?php
            if(!isset($errors) || $errors['nomRue']['valid']){
                echo('is-valid');
            } else {
                echo('is-invalid');
            }
            ?>"         <?php
            if(isset($errors) && $errors['nomRue']['valid']){
                echo('value="'.$errors['nomRue']['value'].'"');
            }?> id="nomRue">
            <div class="invalid-feedback">
                <?php
                if(isset($errors) && !$errors['nomRue']['valid']){
                    echo($errors['nomRue']['cause']);
                }?>
            </div>
        </div>

        <div class="col-md-12">
            <label for="ville" class="form-label">Ville</label>
            <input type="text" value="<?php echo($restaurant->getVille()); ?>" name="ville" class="form-control <?php
            if(!isset($errors) || $errors['ville']['valid']){
                echo('is-valid');
            } else {
                echo('is-invalid');
            }?>" <?php
            if(isset($errors) && $errors['ville']['valid']){
                echo('value="'.$errors['ville']['value'].'"');
            }?>
                   id="ville">
            <div class="invalid-feedback">
                <?php
                if(isset($errors) && !$errors['ville']['valid']){
                    echo($errors['ville']['cause']);
                }?>
            </div>
        </div>

        <div class="col-12">
            <button class="btn btn-primary" type="submit">Submit form</button>
        </div>
    </form>



<?php
include 'Vue/parts/footer.php'
?>
</div>
<?php
include 'Vue/parts/global/global-scripts.php'
?>

</body>
</html>
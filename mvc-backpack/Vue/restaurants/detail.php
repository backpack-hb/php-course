<html>
<head>
    <?php include 'Vue/parts/global/global-stylesheets.php'; ?>
</head>
<body>
<div class="container">
    <?php
    include 'Vue/parts/menu.php'
    ?>

    <?php
    // Ce code PHP permet simplement d'afficher mon objet restaurant.
    if ($resto) {
        ?>
        <h1>Bienvenue sur la page de <?php echo($resto->getNom());?> !</h1>
        <h2>
            Nous sommes localisé au
            <?php
                $adresse = $resto->getNumeroRue().' '.$resto->getNomRue().' '.$resto->getVille();
                echo($adresse);
            ?>
        </h2>

        <h3>Restaurant de type <?php echo($resto->getCategory()->getNom());?></h3>

        <!--
        Pour revenir en arrière je fais toujours mon lien vers le fichier index.php
        C'est notre routeur toutes les requêtes doivent impérativement passer par lui en MVC
        -->

        <a href="index.php?controller=restaurant&action=list">Revenir en arrière</a>
        <?php
    } else {
        ?>
        <h1>Ce restaurant n'existe pas ou n'existe plus</h1>
        <?php
    }
    ?>



<?php
include 'Vue/parts/footer.php'
?>
</div>
<?php
include 'Vue/parts/global/global-scripts.php'
?>
</body>
</html>
<html>
<head>
    <?php include 'Vue/parts/global/global-stylesheets.php'; ?>
    <?php include 'Vue/parts/table/table-stylesheets.php'; ?>
</head>
<body>
<div class="container">
    <?php
    include 'Vue/parts/menu.php'
    ?>


    <h1>Les categories de l'appli !</h1>

    <a href="index.php?controller=category&action=add">Ajouter une categorie !</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nom</th>
            <th scope="col">Nombre de resto de la catégorie</th>
            <th scope="col">Liste des restos !</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        // Seul PHP de la vue : Affichage de mon tableau de restaurant..
        foreach ($categories as $categ) {
            $liResto = '';
            foreach ($categ->getRestaurants() as $restaurant) {

                $liResto .= '<li>'.$restaurant->getNom().'</li>';
            }
            echo(' <tr>
            <td scope="row">' . $categ->getId() . '</td>
            <td>' . $categ->getNom() . '</td>
              <td>' . count($categ->getRestaurants()) . '</td>
               <td><ol>
                '.$liResto.'
</ol>
</td>
          
            <td>
                <a href="index.php?controller=category&action=edit&id=' . $categ->getId() . '">
                 
                 <button class="btn btn-warning">
                    Editer '.$categ->getNom().'
                </button>
                
                </a>
                <button onclick="changeModalLinkCategory('.$categ->getId().')" data-bs-toggle="modal" data-bs-target="#modalDelete"class="btn btn-danger">Supprimer</button>
            </td>
        </tr>');
        }
        ?>

        </tbody>
    </table>


    <div class="modal" id="modalDelete" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Supprimer une categorie !</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Action irrémédiable !</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <a id="deleteButtonModal">
                        <button id="" type="button" class="btn btn-danger">Save changes</button>
                    </a>

                </div>
            </div>
        </div>
    </div>


    <?php
    include 'Vue/parts/footer.php'
    ?>

    <?php
    include 'Vue/parts/global/global-scripts.php';
    ?>

    <?php
    include 'Vue/parts/table/table-scripts.php';
    ?>

</div>
</body>
</html>
<html>
<head>
    <?php include 'Vue/parts/global/global-stylesheets.php'; ?>
</head>
<body>
<div class="container">
    <?php
    include 'Vue/parts/menu.php'
    ?>

    <h1>Ajouter une categorie !</h1>

    <a href="index.php?controller=category&action=list">Revenir en arrière !</a>

    <form class="row g-3" method="post">
        <div class="col-md-12">
            <label for="categoryName" class="form-label">Nom de la catégorie</label>
            <input type="text" id="categoryName" name="nom" class="form-control <?php
            if(isset($errors) && $errors['nom']['valid']){
                echo('is-valid');
            } else {
                echo('is-invalid');
            }
            ?>"
                <?php
                if(isset($errors) && $errors['nom']['valid']){
                    echo('value="'.$errors['nom']['value'].'"');
                }
                ?> id="nomRestaurant">
            <div class="invalid-feedback">
                <?php
                if(isset($errors) && !$errors['nom']['valid']){
                    echo($errors['nom']['cause']);
                }?>
            </div>
        </div>

        <div class="col-12">
            <button class="btn btn-primary" type="submit">Submit form</button>
        </div>
    </form>





    <?php
    include 'Vue/parts/footer.php'
    ?>
</div>

<?php
include 'Vue/parts/global/global-scripts.php'
?>
</body>
</html>
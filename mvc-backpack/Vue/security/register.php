<html>
<head>
    <?php include 'Vue/parts/global/global-stylesheets.php'; ?>
    <link href="Public/css/login.css" rel="stylesheet">
</head>

<body>
<div class="wrapper fadeInDown">
    <div id="formContent">
        <!-- Login Form -->
        <form method="post" enctype="multipart/form-data">
            <input type="text" id="username" class="fadeIn second" name="username" placeholder="Nom d'utilisateur"><br>
            <?php
             if(isset($errors) && array_key_exists("username", $errors) && $errors['username']['valid'] == false){
                 echo("<span class='text-danger'>".$errors["username"]["cause"]."</span>");
             }
            ?>
            <input type="password" id="password" class="fadeIn third" name="password" placeholder="Mot de passe"><br>

            <?php
            if(isset($errors) && array_key_exists("password", $errors) && $errors['password']['valid'] == false){
                echo("<span class='text-danger'>".$errors["password"]["cause"]."</span>");
            }
            ?>

            <input type="password" id="password" class="fadeIn third" name="password_confirm" placeholder="Confirmation du mot de passe"><br>
            <?php
            if(isset($errors) && array_key_exists("password_confirm", $errors) && $errors['password_confirm']['valid'] == false){
                echo("<span class='text-danger'>".$errors["password_confirm"]["cause"]."</span>");
            }
            ?>

            <input type="text" id="nom" class="fadeIn third" name="nom" placeholder="Nom"><br>
            <?php
            if(isset($errors) && array_key_exists("nom", $errors) && $errors['nom']['valid'] == false){
                echo("<span class='text-danger'>".$errors["nom"]["cause"]."</span>");
            }
            ?>

            <input type="text" id="prenom" class="fadeIn third" name="prenom" placeholder="Prenom"><br>

            <?php
            if(isset($errors) && array_key_exists("prenom", $errors) && $errors['prenom']['valid'] == false){
                echo("<span class='text-danger'>".$errors["prenom"]["cause"]."</span>");
            }
            ?>

            <input type="text" id="email" class="fadeIn third" name="email" placeholder="Email"><br>

            <?php
            if(isset($errors) && array_key_exists("email", $errors) && $errors['email']['valid'] == false){
                echo("<span class='text-danger'>".$errors["email"]["cause"]."</span>");
            }
            ?>

            <input type="file" class="fadeIn third mt-3 mb-3" name="profile_picture">

            <?php
            if(isset($errors) && array_key_exists("profile_picture", $errors) && $errors['profile_picture']['valid'] == false){
                echo("<span class='text-danger'>".$errors["profile_picture"]["cause"]."</span>");
            }
            ?>

            <input type="submit" class="fadeIn fourth" value="M'enregistrer !">
        </form>

        <!-- Remind Passowrd -->
        <div id="formFooter">
            <a class="underlineHover" href="index.php?controller=security&action=login">Revenir au login !</a>
        </div>

    </div>
</div>

<?php
include 'Vue/parts/global/global-scripts.php'
?>
</body>
</html>
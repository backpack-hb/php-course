<html>
<head>
    <?php include 'Vue/parts/global/global-stylesheets.php'; ?>
    <link href="Public/css/login.css" rel="stylesheet">
</head>

<body>
<div class="wrapper fadeInDown">
    <div id="formContent">
        <!-- Login Form -->
        <form method="post">
            <input type="text" id="login" class="fadeIn second" name="login" placeholder="login">
            <input type="password" id="password" class="fadeIn third" name="password" placeholder="password">
            <input type="submit" class="fadeIn fourth" value="Se connecter">
        </form>

        <?php
            if(count($errors) != 0){
                foreach ($errors as $error) {
                    echo('<span class="text-danger">'.$error.'</span>');
                }
            }
        ?>
        <!-- Remind Passowrd -->
        <div id="formFooter">
            <a class="underlineHover" href="index.php?controller=security&action=register">M'enregistrer !</a>
        </div>

    </div>
</div>

<?php
include 'Vue/parts/global/global-scripts.php'
?>

</body>
</html>
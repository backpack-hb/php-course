<?php

/*
 * Fichier index.php
 * Ce fichier est le point d'entrée de notre application.
 * Toutes les requêtes envoyées à notre application passeront par ce fichier
 * En fonction des paramètres GET envoyés, nous redirigerons l'utilisateur vers le bon controlleur
 */

/*
 * Ici nous avons inclu toutes les classes nécessaire au fonctionneent de l'application.
 * A chaque nouvelle classe créé, il faudra l'ajouter dans ce fichier
 */
require "autoload.php";

// Le cas ou on a pas de variable GET, on redirige l'utilisateur vers une URL fonctionnelle.
// Ce sera la homepage de notre application
if(empty($_GET)){
    header("Location: index.php?controller=restaurant&action=list");
}

// Si on a la variable $_GET['controller'] = restaurant index.php?controller=restaurant
// On cré un nouvel objet Restaurant controller.
// On retrouvera ici toutes les URLS de notre application relatives au restaurant
if($_GET['controller'] == 'restaurant'){
    // Instancie un nouveau controlleur
    $controller = new RestaurantController();

    // Si notre action est égale à list ?action=list
    if($_GET['action'] == 'list'){
        // On appelle la méthode listRestaurant de notre controller
        $controller->listRestaurant();
    }
    // si notre action vaut détail ?action=detail
    // On appel la méthode détail de notre controller.
    // On lui passe ici un paramètre ID qui permettra de cibler le restaurant que l'on veut afficher.
    if($_GET['action'] == 'detail' && isset($_GET['id'])){
        // On appel la méthode détail de notre controller en veillant à bien envoyer le paramètre id.
        $controller->detail($_GET['id']);
    }

    if($_GET['action'] == 'add'){
       $controller->add();
    }

    if($_GET['action'] == 'delete' && isset($_GET['id'])){
        $controller->remove($_GET['id']);
    }

    if($_GET['action'] == 'edit' && isset($_GET['id'])){
        $controller->edit($_GET['id']);
    }

}

if($_GET['controller'] == 'security'){
    $controller = new SecurityController();
    if($_GET['action'] == 'register'){
        $controller->register();
    }
    if($_GET['action'] == 'login'){
        $controller->login();
    }
    if($_GET['action'] == 'logout'){
        $controller->logout();
    }
}

if($_GET['controller'] == 'category'){
    $controller = new CategoryController();
    if($_GET['action'] == 'list'){

        $controller->list();
    }

    if($_GET['action'] == 'add'){
        $controller->add();
    }

    if($_GET["action"] == 'edit' && isset($_GET['id'])){
        $controller->edit($_GET['id']);
    }

    if($_GET["action"] == 'delete' && isset($_GET["id"])){
        $controller->remove($_GET["id"]);
    }
}
?>
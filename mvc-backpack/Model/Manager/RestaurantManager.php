<?php
// Ce fichier sera en charge d'effectuer des requêtes sur mon serveur MySQL
// Il retournera des objet de type Restaurant
// Il étend de la classe DbManager qui permet de se connecter à la BDD
class RestaurantManager extends DbManager {

    // Ici le constructeur de mon objet RestaurantManager
    public function __construct()
    {
        // Dans ce constructeur, j'appel le constructeur de la classe parent (DbManager)
        parent::__construct();
    }

    // Fonction qui ne prend pas de paramètre et qui retourne un tableau d'objet Restaurant
    public function getAll(){
        // La variable qui sera retournée à la fin. De base c'est un tableau vide
        $arrayRestaurant = [];
        // On prépare notre requete qui va séléctionner tous nos restaurants
        $query = $this->bdd->prepare("SELECT * FROM restaurant");
        $query->execute();
        // On réccupére notre resultat MySQL
        $results = $query->fetchAll();

        // Problème : Ce resultat est sous forme de tableau numérotés contenant des tableaux clés -> valeurs
        // Notre manager doit retourner des tableaux d'objets
        // Je parcours les tableaux clé valeurs.
        foreach ($results as $result){
            // J'utilise les valeurs de mon tableau clé valeur pour construire des objets restaurant à partir
            // du constructeur de ma table restauran(t
            $arrayRestaurant[] = new Restaurant($result["nom"], $result["numero_rue"],
                $result["nom_rue"], $result["ville"], null ,$result["id"]);
        }

        // Ici je retourne ce tableau d'objet à mon controleur
        return $arrayRestaurant;
    }


    // Fonction qui ne prend pas de paramètre et qui retourne un tableau d'objet Restaurant
    public function getOneFull($id){

        // La variable qui sera retournée à la fin. De base c'est un tableau vide
        $arrayRestaurant = [];
        // On prépare notre requete qui va séléctionner tous nos restaurants
        $query = $this->bdd->prepare("SELECT * FROM restaurant as resto JOIN category as categ on categ.id=resto.id_category WHERE resto.id = :id");
        $query->execute(["id"=> $id]);
        // On réccupére notre resultat MySQL
        $result = $query->fetch();

        $category = new Category($result[7],$result["id_category"]);

        $restaurant = new Restaurant($result[1], $result[2], $result[3], $result[4], $category, $result[0]);

        return $restaurant;
    }

    // Méthode qui prend en paramètre un id. Elle retourne l'objet restaurant qui correspond à cet ID
    // Si aucun objet restaurant ne correspond à cet id, elle retourne null.
    public function getOne($id){
        // Je déclare une variable resto nulle
        $resto = null;

        // Je fais une requête préparée qui va selectionner un resto en fonction de son id
        $query = $this->bdd->prepare("SELECT * FROM restaurant WHERE id = :id");
        // Je passe mon paramètre id dans mon objet PDO
        $query->execute(['id'=> $id]);
        // Je réccupére mes resultats sour forme de tableau clé valeur.
        $result = $query->fetch();

        // Si j'ai un resultat je transforme mon tableau clé => valeur en objet restaurant
        if($result) {
            $resto =  new Restaurant($result["nom"], $result["numero_rue"],
                $result["nom_rue"], $result["ville"],null, $result["id"]);
        }

        // Je retourne retourne mon objet restaurant
        return $resto;
    }

    public function add(Restaurant $restaurant){

        $query = $this->bdd->prepare("INSERT INTO restaurant (nom, numero_rue, nom_rue, ville, id_category)
                                            VALUES (:nom, :numero_rue, :nom_rue, :ville, :id_categ)");
        $query->execute([
            'nom'=> $restaurant->getNom(),
            'numero_rue'=> $restaurant->getNumeroRue(),
            'nom_rue'=> $restaurant->getNomRue(),
            'ville'=> $restaurant->getVille(),
            'id_categ'=> $restaurant->getCategory()->getId()
        ]);
    }

    public function remove($id){
        $query = $this->bdd->prepare("DELETE FROM restaurant WHERE id = :id");
        $query->execute(['id'=> $id]);
    }

    public function edit(Restaurant $restaurant){
        $query = $this->bdd->prepare('UPDATE restaurant
        SET nom = :nom, numero_rue = :numero_rue, nom_rue = :nom_rue, ville = :ville
        WHERE id = :id');

        $query->execute(
            [
                'nom'=> $restaurant->getNom(),
                'numero_rue'=> $restaurant->getNumeroRue(),
                'nom_rue'=> $restaurant->getNomRue(),
                'ville'=> $restaurant->getVille(),
                'id'=> $restaurant->getId()
            ]);
    }

    public function findByCategory($id){
        $query = $this->bdd->prepare("SELECT * FROM restaurant WHERE id_category = :id");
        $query->execute(["id"=>$id]);

        $results = $query->fetchAll();

        foreach ($results as $result){

            $arrayRestaurant[] = new Restaurant($result["nom"], $result["numero_rue"],
                $result["nom_rue"], $result["ville"], null ,$result["id"]);
        }

        return $arrayRestaurant;
    }
}
<?php
    class CategoryManager extends DbManager {
        private $restaurantManager;

        public function __construct()
        {
            parent::__construct();
            $this->restaurantManager = new RestaurantManager();
        }

        public function getAll(){
            $categories = [];
            $query = $this->bdd->prepare("SELECT * FROM category");
            $query->execute();

            $resultats = $query->fetchAll();

            foreach ($resultats as $resultat){
                $categories[] = new Category($resultat["nom"],null, $resultat["id"]);
            }

            return $categories;
        }

        public function getAllFull(){
            $categories = [];
            $query = $this->bdd->prepare("SELECT * FROM category");
            $query->execute();

            $resultats = $query->fetchAll();

            foreach ($resultats as $resultat){
                $restos = $this->restaurantManager->findByCategory($resultat["id"]);


                $categories[] = new Category($resultat["nom"], $restos , $resultat["id"]);
            }

            return $categories;
        }

        public function add(Category $category){
            $query = $this->bdd->prepare("INSERT INTO category (nom) VALUES (:nom)");
            $query->execute(["nom"=> $category->getNom()]);
        }

        public function findOne($id){
            $categ = null;
            $query = $this->bdd->prepare("SELECT * FROM category WHERE id = :id");
            $query->execute(["id"=> $id]);
            $resultat = $query->fetch();

            if($resultat){
                $categ = new Category($resultat["nom"],null,  $resultat["id"]);
            }

            return $categ;
        }

        public function update(Category $category){
            $query = $this->bdd->prepare("UPDATE category SET nom = :nom WHERE id = :id");
            $query->execute([
                'nom'=> $category->getNom(),
                'id'=> $category->getId()
            ]);
        }

        public function remove($id){
            $query = $this->bdd->prepare("DELETE FROM category WHERE id = :id");
            $query->execute(["id"=> $id]);
        }

        public function findByNom($nom){
            $categ = null;
            $query = $this->bdd->prepare("SELECT * FROM category WHERE nom = :nom");
            $query->execute(["nom"=> $nom]);
            $resultat = $query->fetch();

            if($resultat){
                $categ = new Category($resultat["nom"], $resultat["id"]);
            }
            return $categ;
        }
    }
?>
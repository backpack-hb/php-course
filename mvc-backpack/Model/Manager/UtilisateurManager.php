<?php
class UtilisateurManager extends DbManager{
    public function findByUsername($username){
        $utilisateur = null;
        $query = $this->bdd->prepare("SELECT * FROM users WHERE nom_utilisateur = :username");

        $query->execute([':username'=> $username]);
        $res = $query->fetch();
        if($res){
            $utilisateur = new Utilisateur($res['nom_utilisateur'], $res['mot_de_passe'],
                $res['nom'], $res['prenom'], $res['email'], $res['image_link'], $res['id']);
        }
        return $utilisateur;
    }

    public function add(Utilisateur $utilisateur){
        $query = $this->bdd->prepare(
            "INSERT INTO users (nom_utilisateur, mot_de_passe, nom, prenom, email, image_link)
                    VALUES (:username, :mot_passe, :nom, :prenom, :email, :image)");
        $query->execute([
            "username"=> $utilisateur->getUsername(),
            "mot_passe"=> $utilisateur->getPassword(),
            "nom"=> $utilisateur->getNom(),
            "prenom"=> $utilisateur->getPrenom(),
            "email"=> $utilisateur->getEmail(),
            "image"=> $utilisateur->getProfilePicture()
        ]);
    }
}
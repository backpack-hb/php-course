<?php
class Category {
    private $id;
    private $nom;
    private $restaurants;

    public function __construct($nom, $restaurants , $id = null)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->restaurants = $restaurants;
    }

    public function getRestaurants()
    {
        return $this->restaurants;
    }

    public function setRestaurants($restaurants)
    {
        $this->restaurants = $restaurants;
    }


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }
}
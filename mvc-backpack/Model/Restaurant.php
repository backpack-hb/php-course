<?php
class Restaurant{
    private $id;
    private $nom;
    private $numeroRue;
    private $nomRue;
    private $ville;
    private $category;

    public function __construct($nom, $numeroRue, $nomRue, $ville, Category $category = null, $id=null){
        $this->id = $id;
        $this->nom = $nom;
        $this->numeroRue = $numeroRue;
        $this->nomRue = $nomRue;
        $this->ville = $ville;
        $this->category = $category;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory(Category $category)
    {
        $this->category = $category;
    }

    public function getNumeroRue()
    {
        return $this->numeroRue;
    }

    public function setNumeroRue($numeroRue)
    {
        $this->numeroRue = $numeroRue;
    }

    public function getNomRue()
    {
        return $this->nomRue;
    }

    public function setNomRue($nomRue)
    {
        $this->nomRue = $nomRue;
    }

    public function getVille()
    {
        return $this->ville;
    }

    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getNom(){
        return $this->nom;
    }

    public function setNom($nom){
        $this->nom = $nom;
    }
}
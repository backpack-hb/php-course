###### **Notions du jour (21 Octobre) :**

- Utilisation de PHP depuis un navigateur web en passant par un serveur web 
- Utilisation de PHP depuis une commande console 
- Afficher des chaines de caractères depuis PHP
- PHP : Les bases ! 

###### **Notions du jour (22 Octobre) :**

- Conditions
  - Opérateurs (==, ===, <, >, <=, <=, !=, !==)
  - Conditions multiples (utilisation du &&, du AND, du || ou du OR)
- Ternaires

```php
$isAdmin = true;

$stringAdmin = ($isAdmin === true)? 'Je suis admin' : 'Je ne suis pas admin'
```
- Boucles

```php
<?php
foreach($elements as $element){
    var_dump($element);
}
```
###### **Notions du jour (22 Octobre) :**
- Opérateurs (==, ===, <, >, <=, <=, !=, !==)
- Conditions multiples (utilisation du &&, du AND, du || ou du OR)
- Ternaires





- Connexion à une BDD 
```php
<?php

// Je demande a mon application PHP de se connecter à mon serveur MySQL
$connexion = new PDO('mysql:host='.$dbUrl.';port='.$port.';dbname='.$dbName, $user, $password);

// Je prépare mon serveur PHP à envoyer une requête SQL à ma BDD (celle que l'on a créé a la ligne 17)
$query = $connexion->prepare('SELECT * FROM favorite_restaurant');

// J'execute ma requête
$query->execute();

// Réccupérer tous les résultats de ma requête sous forme de tableau
$arrayBar = $query->fetchAll();  

?> 
```
- Création de BDD 

````mysql
CREATE TABLE `favorite_restaurant` (
`id` int NOT NULL AUTO_INCREMENT,
`nom` varchar(250) NOT NULL,
`num_rue` int NOT NULL,
`nom_rue` varchar(250) NOT NULL,
`ville` varchar(250) NOT NULL,
`code_postal` varchar(250) NOT NULL,
`type` varchar(250) NOT NULL,
`image_link` varchar(250) NOT NULL,
`star` int NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
````
RECAP Formulaires / vérifications 

- Création d'un fichier PHP restaurants-add.php
- Liens de nos fichiers restaurants.php et restaurants-add.php 
  on a modifié tout ça dans  notre menu avec des liens a qui ont un attribut href = au nom du fichier php
- restaurant-add.php 

###### **Notions du jour (25 Octobre) :**

- Etude des variables super globales (GET/POST/SESSION).
- GET : Utiliser pour envoyer des données dans l'URL
- POST : Utiliser pour envoyer des données via un formulaire. Les données sont envoyées dans le corps de la requête
- SESSION : Permet de stocker de ensemble clé => valeur dans la mêmoire du serveur web. Elles ont un temps de vie maximum. Au dela, elle seront automatiquement supprimées. Elles sont aussi supprimé quand on utiliser la fonction php session_destroy();

```php
<?php

/*
 *  SUPER GLOBALE GET
 */
 
// index.php?nom=delorme&prénom=aurelien
// Affichage des données envoyés dan l'URL (Super globale $_GET)
// Nous aurons un tableau avec la clé nom = à delorme et la clé prénom = à aurelien
var_dump($_GET);

/*
 *  SUPER GLOBALE POST
 *  Contient les données envoyées dans un formulaire 
 *  Chaque ensemble clé => valeur de ce tableau représentera 
 *  les inputs envoyés dans mon formulaire via leur attribut "name"
 *  Utilisation de l'attribut method 
 *  <form method="post"></form>
 */
 var_dump($_POST);
 
/*
 *  SUPER GLOBALE POST
 *  Contient les données envoyées dans un formulaire 
 *  Chaque ensemble clé => valeur de ce tableau représentera 
 *  les inputs envoyés dans mon formulaire via leur attribut "name"
 *  Utilisation de l'attribut method 
 *  <form method="post"></form>
 */
 
//Enregistrement de données en session
session_start();
$_SESSION['username'] = 'Aurélien';

 
// Affichage des données en session
session_start();
var_dump($_SESSION);

// Suppression de nos données de session (déconexion)
session_start();
session_destroy();

```

Effectuer une redirection en PHP : 

```php
header("Location: page.php");
```

- Création du fichier qui contiendra notre formulaire de connexion : 

  https://gitlab.com/backpack-hb/php-course/-/blob/main/businesscase/login.php
  

- Création d'un fichier qui contiendra les fonctions relatives à l'authentification : 
  https://gitlab.com/backpack-hb/php-course/-/blob/main/businesscase/functions/user_functions.php. Il contient actuellement 2 fonctions :
  - La première isConnected() retourne un booléen si notre utilisateur est connecté.
  - La deuxieme : Retourne un tableau clé valeur représentant un utilisateur en fonction de son username. 
  
- Modification du menu (menu.php -> https://gitlab.com/backpack-hb/php-course/-/blob/main/businesscase/parts/menu.php)

```php
// Ce code permet d'afficher les bons boutons dans le menu.
// Si le retour de la fonction isConnected
// dans le fichier user_functions.php est égal à true,
// j'affiche les boutons Mon compte, Me déconnecter
// et mon nom d'utilisateur'
// Sinon, j'affiche un lien pour me connecter. Il renvoie vers le formulaire 
// de login
if(isConnected()){
   echo(' <li class="nav-item"><a class="nav-link" href="account.php">Mon compte</a></li>
         <li class="nav-item"><a class="nav-link" href="logout.php">Me déconnecter</a></li>');
   echo('<li class="nav-item"><a class="nav-link" href="account.php">Bonjour '.$_SESSION['username'].'</a></li>');
} else {
    // Sinon, j'affiche un lien qui envoi vers mon formulaire de connexion
    echo(' <li class="nav-item"><a class="nav-link" href="login.php">Me connecter</a></li>');
}
```
###### **Notions du jour (2 Novembre) :**

Objet
- Il possède des attributs et des mèthodes
- Sert à organiser son code, gagner du temps
- Pour créer un nouvel objet en PHP on utilise le mot clé new

Attribut
- Les attributs sont les caractéristiques de notre objet

````php
class Voiture{
    private $attribut1; 
    public $attribut2;
}

````
- Pour créer un attribut on lui assigne une visibilité (privé, public, protected) ensuite son nom précédé par un "$"


Méthode
- Ce sont les fonctionnalité de notre objet. Elle pourront effectuer des
actions
  
````php
class Voiture{
    public function accelerer(){
        echo('plus vite !');
    }
    private function accelerer2(){
        echo('plus vite !');
    }
}
````
- Pour créer une méthode, on utilise la visibilité (public, private, protected). On ajoute le mot clé function 
On met le corps de la fonction entre moustaches "{}"
  
Classe
- Permet de définir un objet c'est comme un moule
- On retrouve les mèthodes et attributs de l'objet

```php
class NomDeLaClasse{

}
```
Utilisation du mot clé class, suivi du nom de la classe ! 
Tout fichier étant une classe php sera écrit comme ça : NomDeLaClasse.php
Toute les classes seront nommés comme ça : NomDeLaClasse

Développement OO 
- Facilité lutilisation 
- gagner du temps 
- facilite la manipulation des données 
- Travail plus organisé 

Avoir des objets qui vont interagir ensemble

Encapsulation
- Permet de donner accès ou non à un attribut ou a une méthode en dehors de notre objet

- Il existe 3 visibilités : 
  - public (accessible en dehors de notre objet)
  - private (seulement accessible dans notre classe et au sein de notre objet)
  - protected accessible dans notre classe et les classes qui en héritent

Abstraction
- On cré une classe abstraite. On empêche la création d'une nouvelle instance
````php
<?php
abstract class Voiture{

}

new Voiture();
````

Ceci ne marchera pas ! On ne peut pas instancier une classe abstraite

- On se servira de ces clases avec de l'héritage. Elle est pour le moment incomplète
- Son code pourra être partagé avec plusieurs classes enfant

Héritage
- Pour éviter de dupliquer du code source
- Permet de factoriser le code 
- On cré une classe parent qui partagera ses méthodes et attributs avec toutes les classes enfants

```php
  class Vehicule(){
  
  }

  class Voiture extends Vehicule{
  
  }

```
- Il faut faire attention, si on a la même fonction dans l'enfant et le parent, c'est la méthode de l'enfant qui sera prioritaire.

````php
  class Vehicule(){
    function accellerer(){
        echo('Ma classe parent accelere');
    }
  }

  class Voiture extends Vehicule{
     function accelerer(){
        echo('Ma classe enfant accelere,');
     }
  }

// Affichera Ma classe enfant accelere
new Voiture();

  class Vehicule(){
    function accellerer(){
        echo('Ma classe parent accelere');
    }
  }

  class Voiture extends Vehicule{
     function accelerer(){
        parent::accelerer();
        echo('Ma classe enfant accelere');
     }
  }

// Affichera Ma classe parent accelere, Ma classe enfant accelere
new Voiture();


````

Méthodes et attributs statiques
- Les méthodes et attributs statiques sont accessible sans créer de nouveaux objets
- Elle n'appartiennent pas à notre objet mais à notre Classe
- pour les créer on utilise : visibilité static nomAttribut

```php
<?php
    class Voiture {
        public static $type = 'SUV';
    }
    
    Voiture::$type;
?>
```

Pour réccupérer un attribut static on utilise nomdelaclasse:nomdelattribut

###### **Notions du jour (3 Novembre) :**

Interfaces 

Sert à donner un cadre pour les classes qui l'implémentent
Une classe qui implémente une interface devra avoir toutes les méthodes décrites dans l'interface.

Pour créer une interface
- On utilise le mot clé interface 
- On écrit la liste des fonctions que devra implémenter les classes qui utiliseront notre interface
````php
interface CrudInterface{
    public function add();
}
````

- Utilisation de notre interface : Cette classe devra implémenter la fonction add avec 0 paramètres

````php
class RestaurantCrud implements CrudInterface{
    // J'ai bien ma fonction add donc tout est OK ! Sinon j'aurais eu une erreur
    public function add(){
        
    }
}
````

- On peut implémenter plusieurs interface 
- Une interface pouvait hériter d'une autre interface

Mèthodes magiques 

- Mèthode qui se déclanche lors d'un événement précis. Nous n'avons pas besoin de les appeler pour qu'elles se déclanchent
- Toutes les méthodes magiques commencent par 2 underscores. 

Les mèthodes magiques 

construct 

Déclanché quand on instancie (cré) un nouvel objet avec le mot clé new 

Classe avec un constructeur : 

````php
class Session{
    public function __construct(){
        echo('Hello je construit mon objet');
    }
}
````

Elément déclancheur de la méthode __construct 

````php
$element = new Session();
````


destruct

- Est appelé lorsque l'on supprime un objet
- Est toujours appelé à la fin d'un script php si l'objet n'a pas été supprimé avant. Cela libére la mémoire serveur

````php
class Session {
    public function __destruct(){
        echo('Je détruit mon objet !');
    }
}
````

````php
$session = new Session();
// Ceci appel mon destructeur
unset($session);
// Si je ne l'avais pas mis il aurait quand même été appelé en fin de script
````
- get 

Elle est appelé quand on essaie d'acceder à un attribut inaccessible (private, protected, inexistant)
- Elle prend un paramètre le nom de l'attribut inaccessible que l'on souhaite retrouver
- Pour la mettre en place : 
````php
  class Session {
    public function __get($name){
        echo($name);
    }
  }
````
Pour l'utiliser : 

````php
  $session = new Session();

  // affichera test 
 echo($session->test);
 
 // affichera toto 
 echo($session->toto);

````

set

- Elle est appelée quand on essaie de mettre à jour un attribut inaccessible 
- Elle prend 2 paramètres le nom de l'attribut auxquel on a voulu assigner une valeur et la valeur que l'on a voulu assigner 

````php
  class Session{
    public function __set($name, $value){
        // Affichera le nom de l'attribut
        echo($name);
        // Affichera la valeur que l'on a voulu lui donner
        echo($value);
    }
  }
````

````php
  $session = new Session();
  // Ceci affichera test puis 1
  $session->test = 1;  
````

isset

- Elle est appelé quand on appel la fonction php isset (vérifie qu'un attribut existe) ou empty (elle vérifie qu'un attribut n'est pas vide) sur un attribut inaccessible. 
- Elle prend un paramètre le nom de l'attribut inaccesible sur lequel la fonction isset/empty à été appelé 

````php
  class Session {
        public function __isset($name){
            echo($name);
        }
  }
````
````php
  $session = new Session();
  // Affichera attributInaccessible
  isset($session->attributInaccessible);
````

unset

- Elle est appelé quand on appel la fonction php unset (supprime une variable)  sur un attribut inaccessible.
- Elle prend un paramètre le nom de l'attribut inaccesible sur lequel la fonction unset à été appelé

````php
  class Session {
        public function __unset($name){
            echo($name);
        }
  }
````
````php
  $session = new Session();
  // Affichera attributInaccessible
  unset($session->attributInaccessible);
````

<?php
// Ajout de la classe Restaurant php dans notre projet on pourra l'utiliser
require 'Model/Restaurant.php';
require 'Model/Utilisateur.php';

// Ajout de notre classe DbManager
// Cette classe permettre de gérer la connexion à ma BDD
require 'Manager/DbManager.php';

// Ajout de notre classe RestaurantManager
// Cette classe contiendra les méthodes de mise à jour de la BDD
require 'Manager/RestaurantManager.php';
require 'Manager/UtillisateurManager.php';


// Créer un nouvel utilisateur
$userManager = new UtillisateurManager();
/*$user = new Utilisateur("toto", "aurelien");

$existingUser = $userManager->getByUsername($user->getUsername());

if(is_null($existingUser)){
    $user = $userManager->register($user);
    echo('<h1>Vous avez enregistré l\'utilisateur'.$user->getUsername().'. Il a l\'id '.$user->getId().'</h1>');
}*/

// Je vais essayer de connecter un utilisateur.
$user = $userManager->getByUsername("aurelien");
$password = "aurelien";

$userIsOk = password_verify($password, $user->getPassword());

if($userIsOk){
    $session->username = "aurelien";
    echo($session->username);
}

// Je vais essayer de l'enregistrer


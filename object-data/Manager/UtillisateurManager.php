<?php
class UtillisateurManager extends DbManager {

    public function register(Utilisateur $utilisateur): Utilisateur {
        $utilisateur->setPassword(password_hash($utilisateur->getPassword(), PASSWORD_DEFAULT));

        $query = $this->bdd->prepare("INSERT INTO utilisateur(username, password)
                            VALUES (:username, :password)");

        $query->execute(['username'=> $utilisateur->getUsername(), 'password'=> $utilisateur->getPassword()]);
        $id = $this->bdd->lastInsertId();

        $utilisateur->setId($this->bdd->lastInsertId());

        return $utilisateur;
    }

    public function getByUsername($username): ?Utilisateur {

        $user = null;
        $query = $this->bdd->prepare('SELECT * FROM utilisateur WHERE username = :username');
        $query->execute(
            ['username'=> $username]
        );

        $result = $query->fetch();

        if($result){
            $user = new Utilisateur($result["username"], $result["password"], $result["id"]);
        }


        // Je retourne maintenant mon tableau d'objet
        return $user;
    }
}
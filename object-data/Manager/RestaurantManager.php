<?php

    // Cette classe étend DbManager
    // Lors de la création d'un nouveau RestaurantManager, je passse dans le constructeur parent
    // J'initialise ma BDD
    class RestaurantManager extends DbManager {

        // Réccupéres tous les enregistrement de ma BDD
        // Cette fonction doit retourner un tableau d'objets
        public function getAll(): array {
            // Requête qui selectionne tout
            $query = $this->bdd->prepare('SELECT * FROM restaurants');
            $query->execute();
            $results = $query->fetchAll();

            // Ici la variable $results est un tableau de tableau
            // Je préfére un tableau d'objet en POO !

            // J'initialise un tableau vide
            $arrayObject = [];

            // Je parcours mes resultats
            // Ci-dessous : Processus d'hydratation
            foreach ($results as $res){
                // Pour chaque resultat, je cré un nouvel objet de type Restaurant et je l'ajoute dans mon tableau
                $arrayObject[] = new Restaurant($res['nom'], $res['num_rue'], $res['ville'], $res['code_postal'],
                    $res['type'], $res['image_link'], $res['star'], $res['slug'], $res['email'], $res['id']);
            }

            // Je retourne maintenant mon tableau d'objet
            return $arrayObject;
        }

        // Cette fonction retourne un objet Restaurant
        // Elle prend en paramètre un id
        // Elle retourne un objet Restaurant
        public function getOne($id): Restaurant {

            // Ici je récupére mon resultat sous forme de tableau
            $query = $this->bdd->prepare('SELECT * FROM restaurants WHERE id = :id');
            $query->execute(['id'=> $id]);
            $result = $query->fetch();

            // Je le transforme en objet via le constructeur de la classe Restaurant
            // Je le retourne
            return new Restaurant($result['nom'], $result['num_rue'], $result['ville'], $result['code_postal'],
                $result['type'], $result['image_link'], $result['star'], $result['slug'], $result['email'], $result['id']);

        }

        // Elle prend en paramètre un objet de type restaurant
        // Elle va l'insérer dans notre BDD
        public function add(Restaurant $restaurant): void {

            // Préparation de la requête d'insertion
            $query = $this->bdd->prepare("INSERT INTO restaurants(nom, num_rue, email, type, image_link)
                            VALUES (:nom, :num_rue, :email, :type, :image_link)");
            // On execute en lui passant les mèthodes permettant l'accès aux attributs qui sont privé
            // On ne lui passe pas d'ID car c'est la DB qui va le générer
            $query->execute(
                [
                    'nom'=> $restaurant->getNom(),
                    'num_rue'=> $restaurant->getNumRue(),
                    'email'=> $restaurant->getEmail(),
                    'type'=> $restaurant->getType(),
                    'image_link'=> $restaurant->getImageLink()
                ]);
        }

        // Notre fonction d'update
        // Prend en paramètre un objet de type Restaurant
        public function update(Restaurant $restaurant): void {
            // Fait la requête préparé
            $query = $this->bdd->prepare("UPDATE restaurants
                            SET nom = :nom, num_rue = :num_rue, email = :email, type = :type, image_link = :image_link
                            WHERE id = :id");

            // Attribuer les champs qui sont stockés dans l'objet restaurant
            $query->execute(
                [
                    'nom'=> $restaurant->getNom(),
                    'num_rue'=> $restaurant->getNumRue(),
                    'email'=> $restaurant->getEmail(),
                    'type'=> $restaurant->getType(),
                    'image_link'=> $restaurant->getImageLink(),
                    'id'=> $restaurant->getId()
                ]);

        }

        // Elle prend un id
        public function delete($id): void {
            // Elle fait la requête préparé pour supprimer cet id
            $query = $this->bdd->prepare("DELETE FROM restaurants WHERE id = :id");
            $query->execute(['id'=> $id]);
        }
    }
?>
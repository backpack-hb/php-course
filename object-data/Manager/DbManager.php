<?php
    // Classe abstraite
    // On ne pourra pas l'instancier. Il est impossible de faire un new DbManager
    abstract class DbManager {
        // Cet attribut représente ma bdd
        // Il a une visibilité protected. Seulement les classes qui héritent de DbManager pourront l'utiliser
        protected $bdd;

        // Attributs privés de configuration de ma base de donnée
        private $dbName = 'business_case_php';
        private $host = 'database';
        private $port = '3306';

        // Le constructeur va être appelé si on cré un nouvel objet DbManager
        public function __construct(){
            try {
                // Quand je cré mon nouvel objet DbManager, je me connecte à ma BDD
                // Je met cette connexion dans mon attribut $bdd
                $this->bdd = new PDO('mysql:host='.$this->host.';port='.$this->port.';dbname='.$this->dbName.';charset=utf8',
                    'root', 'tiger');
                $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


            } catch (PDOException $exception){
                // Si il y a une erreur je l'affiche
                throw $exception;
                die();
            }
        }
    }
?>
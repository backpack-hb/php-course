<?php
    class Utilisateur {

        private $id;
        private $username;
        private $password;

        public function __construct($username, $password, $id = null){
            $this->id = $id;
            $this->username = $username;
            $this->password = $password;
        }

        public function getId(): int {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        }

        public function getUsername(): string{
            return $this->username;
        }

        public function setUsername(string $username): void {
            $this->username = $username;
        }

        public function getPassword(): string {
            return $this->password;
        }

        public function setPassword(string $password): void {
            $this->password = $password;
        }

    }
?>
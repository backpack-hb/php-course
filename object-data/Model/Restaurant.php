<?php
    // Représentation objet de la table restaurants de notre BDD
    class Restaurant{

        // On retrouve toutes les colonnes de notre bdd dans des attributs
        // Ces attributs ont une visibilité privé. Il ne seront pas accessible en dehors de l'objet
        private $id;
        private $nom;
        private $numRue;
        private $ville;
        private $codePostal;
        private $type;
        private $imageLink;
        private $star;
        private $slug;
        private $email;

        // Fonction magique __construce
        // Cette méthode magique sera appelée automatiquement quand on créera un nouvel objet avec le mot clé new
        // Dans cette fonction on initialisera tous les attributs de notre objet
        // On termine se constructeur avec un id = null. Dans le cas d'une insertion on le connaitre pas forcément
        public function __construct($nom, $numRue, $ville, $codePostal, $type, $imageLink, $star, $slug, $email, $id = null)
        {
            // Je prend les paramètres envoyés dans le new de mon objet et je les assignes à mes attributs
            $this->id = $id;
            $this->nom = $nom;
            $this->numRue = $numRue;
            $this->ville = $ville;
            $this->codePostal = $codePostal;
            $this->type = $type;
            $this->imageLink = $imageLink;
            $this->star = $star;
            $this->slug = $slug;
            $this->email = $email;
        }

        // Méthode publique qui réccupére l'attribut id de mon objet et le retourne
        // Vu que cet attribut est privé, je suis obligé de passer par cette méthode
        public function getId(): mixed
        {
            return $this->id;
        }


        // Méthode qui permet de mettre à jour l'attribut id de mon objet qui est privé.
        // Vu que cet attribut est privé, je suis obligé de passer par cette méthode
        public function setId(mixed $id): void
        {
            $this->id = $id;
        }


        // même principe que le getId avec le nom
        public function getNom()
        {
            return $this->nom;
        }

        // même principe que le getId avec le nom
        public function setNom($nom): void
        {
            $this->nom = $nom;
        }

        public function getNumRue()
        {
            return $this->numRue;
        }

        public function setNumRue($numRue): void
        {
            $this->numRue = $numRue;
        }

        public function getVille()
        {
            return $this->ville;
        }

        public function setVille($ville): void
        {
            $this->ville = $ville;
        }

        public function getCodePostal()
        {
            return $this->codePostal;
        }


        public function setCodePostal($codePostal): void
        {
            $this->codePostal = $codePostal;
        }

        public function getType()
        {
            return $this->type;
        }


        public function setType($type): void
        {
            $this->type = $type;
        }

        public function getImageLink()
        {
            return $this->imageLink;
        }

        public function setImageLink($imageLink): void
        {
            $this->imageLink = $imageLink;
        }

        public function getStar()
        {
            return $this->star;
        }

        public function setStar($star): void
        {
            $this->star = $star;
        }

        public function getSlug()
        {
            return $this->slug;
        }

        public function setSlug($slug): void
        {
            $this->slug = $slug;
        }

        public function getEmail()
        {
            return $this->email;
        }

        public function setEmail($email): void
        {
            $this->email = $email;
        }
    }
?>
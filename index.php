<html>

<?php
$variable = 'toto';
var_dump(is_null($variable));
$variable = null;
var_dump(is_null($variable));
?>


<?php
$listeCourse = ['pomme', 'poire', 'chocolat', 'poire', 'pomme'];

array_push($listeCourse, 'Nutella');
// Equivalent de : $listeCourse[] = 'Nutella'

var_dump($listeCourse);

var_dump(array_unique($listeCourse));


$variable = true;
unset($variable);
var_dump($variable);



    // Ceci est une variable de type string
    $titre = 'Ma première page en HTML + PHP';
    var_dump($titre);




    $bestFootballClub = 'CF63';
    $bestRugbyTeam = 'ASM';

    // Des variables de type number
    $classementAsm = 10;
    $classementClermontFoot = 14;
    $titre = 'Second Titre';


    $nbEquipesL1 = 20;

    $exemple = "Aujourd'hui il fait beau";
    $citation = "Il a dit \"Bonjour\" aujourd'hui";
    ?>
<head>
    <title><?php echo($titre);?></title>
</head>
<body>

<h1>Les classements clermontois ! </h1>

<h2>
    <?php
        $stringToPrint = $bestFootballClub.' '.' est actuellement '.$classementClermontFoot.' sur '. $nbEquipesL1;

        // C'est pas très jolie a voir !
        echo($bestFootballClub);
        echo(' est actuellement ');
        echo($classementClermontFoot);
        echo(' sur ');
        echo($nbEquipesL1);
        echo('<br>');

        // Ceci est plus joli !!!
        echo($stringToPrint);
    ?>


</h2>

<!-- Ici c'est le H1 Présent dans ma page -->

<?php
    echo('<h1>H1 De la page !</h1>');
?>
<!--// Données pas confidentielles -->


<?php
    // Je peux mettre mon numéro de carte bleu on s'en fou !
    // Un autre commentaire ici !

/*
UN LONG COMMENTAIRE !
Explique pleins de choses sur le fonctionnement de l'application
etc etc ...
*/


?>
<h2>Bienvenue</h2>
</body>
</html>